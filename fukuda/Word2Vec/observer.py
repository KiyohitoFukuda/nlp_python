# -*- coding: utf-8 -*-

import numpy as np
import chainer
import time
import chainer.links as L
import chainer.functions as F
import chainer.initializers as I
import chainer.optimizers as O
import chainer.serializers as S
from chainer import cuda
import filearranger


class W2VObserver():
    def __init__(self, args):
        self.args     = args
        self.arranger = filearranger.FileArranger()


    def calculate_most_similarity(self, vector, candidates, rank_size):
        max_sim = [-1.0] * rank_size
        max_idx = [0] * rank_size
        count   = 0
        for idx, candidate in enumerate(candidates):
            xy = 0.0
            x  = 0.0
            y  = 0.0
            for i in range(len(vector)):
                xy += vector[i] * candidate[i]
                x  += vector[i] ** 2.0
                y  += candidate[i] ** 2.0
            sim = xy / ((x**0.5)*(y**0.5))
            for j in range(rank_size):
                if max_sim[j] < sim:
                    for k in range(rank_size-j-1)[::-1]:
                        max_sim[j+k+1] = max_sim[j+k]
                        max_idx[j+k+1] = max_idx[j+k]
                    max_sim[j] = sim
                    max_idx[j] = idx
                    print("--", "UPDATE", (j+1), "-- ", "Word-ID:", max_idx)
                    break
            if count%50000 == 0:
                print("(", count , "/", len(candidates), ")")
            count += 1
        print("Max Word-ID:", max_idx, "Max Sim:", max_sim)
        return max_sim, max_idx


    def set_vocaburary(self, vocab, re_vocab):
        self.vocab    = vocab
        self.re_vocab = re_vocab



    def get_w2v_evaluation(self, model, b_word, p_word, n_word, rank):
        b_word_num = self.vocab[b_word]
        p_word_num = self.vocab[p_word]
        n_word_num = self.vocab[n_word]
        b_index    = chainer.Variable(np.array([b_word_num], dtype=np.int32))
        p_index    = chainer.Variable(np.array([p_word_num], dtype=np.int32))
        n_index    = chainer.Variable(np.array([n_word_num], dtype=np.int32))
        b_word_vec = model.get_vector(b_index)[0]
        p_word_vec = model.get_vector(p_index)[0]
        n_word_vec = model.get_vector(n_index)[0]
        candidates = []
        for i in range(len(self.vocab)):
            idx      = chainer.Variable(np.array([i], dtype=np.int32))
            c_vector = model.get_vector(idx)[0]
            candidates.append(c_vector)
        new_vector = b_word_vec + p_word_vec - n_word_vec
        max_sim, max_idx = self.calculate_most_similarity(new_vector, candidates, rank)
        print("\n------- ", b_word, "+", p_word, "-", n_word, "-------")
        for i in range(rank):
            print("RANK", (i+1), "Word:", self.re_vocab[max_idx[i]], "\tSim:", max_sim[i])


    def evaluate_word2vec(self, model, epoch, b_word="王様", p_word="女性", n_word="男性", rank=5, is_save=True):
        self.get_w2v_evaluation(model, b_word, p_word, n_word, rank)

        if is_save:
            model_dir = "../MODEL/Word2Vec/"
            self.arranger.make_directory(model_dir)
            model_file = model_dir + "w2v-model_v" + str(self.args.v_size) + "_w" + str(self.args.w_size) + "_s"  + str(self.args.s_size) + "_c" + str(self.args.c_size) + "_b" + str(self.args.b_size) + "_epoch" + str(epoch) + ".npz"
            vector_file = model_dir + "wordvecs_v" + str(self.args.v_size) + "_w" + str(self.args.w_size) + "_s"  + str(self.args.s_size) + "_c" + str(self.args.c_size) + "_b" + str(self.args.b_size) + "_epoch" + str(epoch)

            S.save_npz(model_file, model)

            vec_list = []
            for word_num in self.re_vocab:
                word   = self.re_vocab[word_num]
                index  = chainer.Variable(np.array([word_num], dtype=np.int32))
                vector = model.get_vector(index)[0]
                data = word + "__" + str(word_num)
                for element in vector:
                    data += "__" + str(element)
                vec_list.append(data+"\n")
            self.arranger.write_file(vector_file, vec_list)


    def save_dictionary(self, vocab, count, data_file):
        dict_dir = "../DICT/Word2Vec/"
        self.arranger.make_directory(dict_dir)
        dict_file = dict_dir + data_file + ".dict"

        dict_list = []
        for word, idx in vocab.items():
            dict_info = word + " " + str(idx) + " " + str(count[word]) + "\n"
            dict_list.append(dict_info)
        self.arranger.write_file(dict_file, dict_list)






class D2VObserver():
    def __init__(self, args):
        self.args     = args
        self.arranger = filearranger.FileArranger()


    def calculate_similarity(self, vector1, vector2):
        xy = 0.0
        x  = 0.0
        y  = 0.0
        for i in range(len(vector1)):
            xy += vector1[i] * vector2[i]
            x  += vector1[i] ** 2.0
            y  += vector2[i] ** 2.0
        sim = xy / ((x**0.5)*(y**0.5))
        return sim


    def evaluate_doc2vec(self, doc2vec, sentence="教師は生徒が運動場でサッカーをしているところを発見しました。"):
        test1 = "生徒は運動場でサッカーしているところを教師に発見されました。"
        test2 = "教師は生徒がサッカーしているところを発見できませんでした。"
        test3 = "教師は学生が校庭で球技をしているところを発見した。"
        test4 = "先日の歓迎会はアルバイトで参加できなかった。"

        d_vector0 = doc2vec.get_document_vector(document=sentence)
        d_vector1 = doc2vec.get_document_vector(document=test1)
        d_vector2 = doc2vec.get_document_vector(document=test2)
        d_vector3 = doc2vec.get_document_vector(document=test3)
        d_vector4 = doc2vec.get_document_vector(document=test4)
        sim01 = self.calculate_similarity(d_vector0, d_vector1)
        sim02 = self.calculate_similarity(d_vector0, d_vector2)
        sim03 = self.calculate_similarity(d_vector0, d_vector3)
        sim04 = self.calculate_similarity(d_vector0, d_vector4)
        sim12 = self.calculate_similarity(d_vector1, d_vector2)
        sim13 = self.calculate_similarity(d_vector1, d_vector3)
        sim14 = self.calculate_similarity(d_vector1, d_vector4)
        sim23 = self.calculate_similarity(d_vector2, d_vector3)
        sim24 = self.calculate_similarity(d_vector2, d_vector4)
        sim34 = self.calculate_similarity(d_vector3, d_vector4)

        print()
        print("Similarity of s0 and s1:", sim01)
        print("Similarity of s0 and s2:", sim02)
        print("Similarity of s0 and s3:", sim03)
        print("Similarity of s0 and s4:", sim04)
        print("Similarity of s1 and s2:", sim12)
        print("Similarity of s1 and s3:", sim13)
        print("Similarity of s1 and s4:", sim14)
        print("Similarity of s2 and s3:", sim23)
        print("Similarity of s2 and s4:", sim24)
        print("Similarity of s3 and s4:", sim34)

        print()
