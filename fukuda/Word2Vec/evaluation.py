# -*- coding: utf-8 -*-

import numpy as np

import chainer
import chainer.links as L
import chainer.optimizers as O
import chainer.serializers as S

import myword2vec
import filearranger



class Word2Vec():
    def __init__(self):
        self.arranger  = filearranger.FileArranger()
        self.word2idx  = {}
        self.idx2word  = {}
        self.word_vecs = []


    def construct_w2v(self, data_file):
        data_list = self.arranger.read_file(data_file)
        for data in data_list:
            elements = data.split("__")
            word     = elements[0]
            word_num = int(elements[1])
            vector   = np.array([float(val) for val in elements[2:]])
            self.word2idx[word]     = word_num
            self.idx2word[word_num] = word
            self.word_vecs.append(vector)


    def get_word_vector(self, word=None, word_num=None):
        if word != None:
            word_num = self.word2idx[word]
            return word, word_num, self.word_vecs[word_num]
        elif word_num != None:
            word = self.idx2word[word_num]
            return word, word_num, self.word_vecs[word_num]
        else:
            print("You have to set word or word number!")
            return None, None, None



if __name__ == "__main__":
    model = Word2Vec()
    model.construct_w2v("../MODEL/word2vec_vector.model")
    print(model.get_word_vector(word="私"))
