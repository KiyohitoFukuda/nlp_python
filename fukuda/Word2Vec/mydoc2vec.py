# -*- coding: utf-8 -*-

import math
import numpy as np
import chainer
import chainer.functions as F
import chainer.links as L
import chainer.initializers as I
from gensim.models import doc2vec

import filearranger
import mecab
import cabocha
import myword2vec


class Doc2VecOriginal():
    def __init__(self):
        self.w2v_model = None
        self.w2v_vocab = None
        self.mecab     = None
        self.cabocha   = None
        self.docvecs   = None



    def construct_doc2vec(self, model, vocab, vector_size):
        self.w2v_model = model
        self.w2v_vocab = vocab
        self.mecab     = mecab.Mecab()
        self.cabocha   = cabocha.CaboCha()
        self.w_size    = vector_size
        self.d_size    = int(math.sqrt(self.w_size))
        self.average   = 0.0

        for word in self.w2v_vocab:
            i_data = chainer.Variable(np.array([self.w2v_vocab[word]], dtype=np.int32))
            w_vec  = self.w2v_model.get_vector(i_data)
            self.average += np.absolute(w_vec).mean()
        self.average /= len(self.w2v_vocab)
        print(self.average)


    def get_docvecs(self, sentence):
        self.mecab.set_mecab_data(sentence)
        self.cabocha.set_cabocha_data(sentence)
        words    = self.mecab.get_lemma()
        segments = self.cabocha.get_segment_lemma()
        seg_size = self.cabocha.get_segment_size()
        segvecs  = np.empty((seg_size, self.d_size, self.d_size))
        for i, segment in enumerate(segments):
            segvec = np.empty((self.d_size, self.d_size))
            w_num  = 0
            for j, word in enumerate(segment):
                i_data = chainer.Variable(np.array([self.w2v_vocab[word]], dtype=np.int32))
                temp = self.w2v_model.get_vector(i_data).reshape(self.d_size, self.d_size)
                if j == 0:
                    segvec = temp
                else:
                    segvec = np.dot(segvec, temp.T)
                w_num += 1
            segvecs[i] = segvec.copy()
            segvecs[i] = segvecs[i] / ((self.average*self.d_size)**(w_num-1))

        docvec = np.zeros(self.w_size, dtype=np.float32)
        for i in range(seg_size):
            docvec += segvecs[i].reshape(self.w_size) / seg_size
#        docvec  = np.ones(self.w_size, dtype=np.float32)
#        for i in range(seg_size):
#            docvec *= segvecs[i].reshape(self.w_size)
#        docvec_filter = docvec >= 0.0
#        docvec = docvec * (2*docvec_filter-1)
#        docvec = docvec ** (1/seg_size)
#        docvec = docvec * (2*docvec_filter-1)
        return docvec


    def get_document_vector(self, document=None, word=None):
        if document != None:
            d_vector = self.get_docvecs(document)
            print("Document:", document)
            print("Vector:", d_vector)
            return d_vector
        if word != None:
            i_data = chainer.Variable(np.array([self.w2v_vocab[word]], dtype=np.int32))
            w_vector = self.w2v_model.get_vector(i_data)
            print("Word:", word)
            print("Vector:", w_vector)
            return w_vector





class Doc2VecGensim():
    def __init__(self):
        self.model_gen = None
        self.mecab     = None
        self.cabocha   = None


    def construct_doc2vec(self, model_file):
        self.model_gen = doc2vec.Doc2Vec.load(model_file)
        self.mecab     = mecab.Mecab()
        self.cabocha   = cabocha.CaboCha()


    def get_docvecs(self, sentence):
        self.mecab.set_mecab_data(sentence)
        i_data = " ".join(self.mecab.get_wakati())
        docvec = self.model_gen.infer_vector(i_data)
        return docvec


    def get_document_vector(self, document=None):
        if document != None:
            d_vector = self.get_docvecs(document)
            print("Document:", document)
            print("Vector:", d_vector)
            return d_vector
