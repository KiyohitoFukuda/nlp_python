# -*- coding: utf-8 -*-

import sys
import io
import argparse
import time
import numpy as np
import chainer
import chainer.links as L
import chainer.optimizers as O
import chainer.serializers as S
import downloader
import myword2vec
import mydoc2vec
import filearranger
import converter
import observer


# 引数設定
parser = argparse.ArgumentParser()
parser.add_argument("--m_type", "-m", default="sg", help="Type of word2vec models ('''sg''' or '''cbow''')")
parser.add_argument("--l_type", "-l", default="ne", help="Type of leraning algorithms ('''ne''', '''hs''', and '''so''')")
parser.add_argument("--v_size", "-v", type=int, default=100, help="size of word vector")
parser.add_argument("--w_size", "-w", type=int, default=10, help="window size")
parser.add_argument("--s_size", "-s", type=int, default=10, help="sampling size of negative")
parser.add_argument("--c_size", "-c", type=int, default=20, help="size of minimum word counts")
parser.add_argument("--b_size", "-b", type=int, default=100, help="mini batch size")
parser.add_argument("--epoch", "-e", type=int, default=5, help="Number of epoch")
parser.add_argument("--input", "-i", default="wiki_wakati_u.txt", help="file name for training")
parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU and positive value indicates GPU)')
args = parser.parse_args()

arranger = filearranger.FileArranger()
observer = observer.D2VObserver(args)


# データ読み込み
vocab_max  = 1000000
min_count  = args.c_size
input_file = "../DATA/Word2Vec/" + args.input
dict_file  = "../DICT/Word2Vec/" + args.input + ".dict"

start1 = time.time()
corpus = downloader.NovelLoader(vocab_max, min_count)
#corpus.read(input_file)
#vocab, re_vocab, count    = corpus.make_vocabulary()
vocab, re_vocab, count = corpus.load_vocabulary(dict_file)
vocab_size = corpus.get_vocab_size()
words_size = corpus.get_words_size()
print("vocab_size:", vocab_size)
print("経過時間 (データ読み込み):", str(time.time()-start1))


# モデル読み込み
vector_size = args.v_size
window_size = args.w_size
sample_size = args.s_size
batch_size  = args.b_size
loss_func   = None
model       = None
load_file   = "../MODEL/Word2Vec/w2v-model_v" + str(args.v_size) + "_w" + str(args.w_size) + "_s"  + str(args.s_size) + "_c" + str(args.c_size) + "_b" + str(args.b_size) + "_epoch" + str(args.epoch) + ".npz"

if args.l_type == "ne":
    noise     = [count[word] for word in vocab]
    loss_func = L.NegativeSampling(vector_size, noise, sample_size)
    loss_func.W.data[...] = 0
if args.l_type == "hs":
    HSM       = L.BinaryHierarchicalSoftmax
    tree      = HSM.create_huffman_tree(count)
    loss_func = HSM(vector_size, tree)
    loss_func.W.data[...] = 0
if args.m_type == "sg":
    model = myword2vec.SG(vocab_size, vector_size, loss_func)
if args.m_type == "cbow":
    model = myword2vec.CBOW(vocab_size, vector_size, loss_func)
optimizer = O.Adam(alpha=0.00001)
optimizer.setup(model)


# Doc2Vec_original モデルの構築・評価
S.load_npz(load_file, model)
doc2vec_ori = mydoc2vec.Doc2VecOriginal()
doc2vec_ori.construct_doc2vec(model, vocab, vector_size)
observer.evaluate_doc2vec(doc2vec_ori)


# Doc2Vec_gensim モデルの構築・評価
v_size = 100
w_size = 10
c_size = 5
model_file = "../MODEL/Gen-W2V/d2v-model_v" + str(v_size) + "_w" + str(w_size) + "_c" + str(c_size) + ".model"
doc2vec_gen = mydoc2vec.Doc2VecGensim()
doc2vec_gen.construct_doc2vec(model_file)
observer.evaluate_doc2vec(doc2vec_gen)
