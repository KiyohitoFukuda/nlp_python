# -*- coding: utf-8 -*-

import sys
import io
import argparse
import time
import numpy as np
import chainer
import chainer.links as L
import chainer.optimizers as O
import chainer.serializers as S
import downloader
import myiterator
import myword2vec
import filearranger
import converter
import observer


# 引数設定
parser = argparse.ArgumentParser()
parser.add_argument("--m_type", "-m", default="sg", help="Type of word2vec models ('''sg''' or '''cbow''')")
parser.add_argument("--l_type", "-l", default="ne", help="Type of leraning algorithms ('''ne''', '''hs''', and '''so''')")
parser.add_argument("--v_size", "-v", type=int, default=100, help="size of word vector")
parser.add_argument("--w_size", "-w", type=int, default=10, help="window size")
parser.add_argument("--s_size", "-s", type=int, default=10, help="sampling size of negative")
parser.add_argument("--c_size", "-c", type=int, default=20, help="size of minimum word counts")
parser.add_argument("--b_size", "-b", type=int, default=100, help="mini batch size")
parser.add_argument("--epoch", "-e", type=int, default=5, help="Number of epoch")
parser.add_argument("--input", "-i", default="wiki_wakati_u.txt", help="file name for training")
parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU and positive value indicates GPU)')
args = parser.parse_args()


arranger = filearranger.FileArranger()
gconv    = converter.GPUConverter(args.gpu)
observer = observer.W2VObserver(args)


# データ読み込み
vocab_max  = 1000000
min_count  = args.c_size
input_file = "../DATA/Word2Vec/" + args.input
output_dir = "../DATA/Word2Vec/"
arranger.make_directory(output_dir)
output_file = output_dir + "w2v_data"

start1 = time.time()
corpus = downloader.NovelLoader(vocab_max, min_count)
corpus.read(input_file)
vocab, re_vocab, count    = corpus.make_vocabulary()
data_novel, data_sentence = corpus.make_dataset(vocab)
vocab_size = corpus.get_vocab_size()
words_size = corpus.get_words_size()
print("vocab_size:", vocab_size, "   ", "words_number:", words_size)
with open(output_file, "w") as f:
    f.write(str(vocab_size) + "\n")
    f.write(str(args.v_size) + "\n")
print("経過時間 (データ整形):", str(time.time()-start1))
observer.set_vocaburary(vocab, re_vocab)
observer.save_dictionary(vocab, count, args.input)


# モデル生成
vector_size = args.v_size
window_size = args.w_size
sample_size = args.s_size
batch_size  = args.b_size
loss_func   = None
model       = None

start2 = time.time()
if args.l_type == "ne":
    noise     = [count[word] for word in vocab]
    loss_func = L.NegativeSampling(vector_size, noise, sample_size)
    loss_func.W.data[...] = 0
if args.l_type == "hs":
    HSM       = L.BinaryHierarchicalSoftmax
    tree      = HSM.create_huffman_tree(count)
    loss_func = HSM(vector_size, tree)
    loss_func.W.data[...] = 0
if args.m_type == "sg":
    model = myword2vec.SG(vocab_size, vector_size, loss_func)
if args.m_type == "cbow":
    model = myword2vec.CBOW(vocab_size, vector_size, loss_func)
optimizer = O.Adam(alpha=0.0001)
optimizer.setup(model)
model = gconv.convert_model(model)


# モデル学習 (Training)
model_dir = "../MODEL/Word2Vec/"
arranger.make_directory(model_dir)
model_file  = model_dir + "w2v-model_v" + str(vector_size) + "_w" + str(window_size) + "_s" \
            + str(sample_size) + "_c" + str(min_count) + "_b" + str(batch_size) + "_epoch"
vector_file = model_dir + "wordvecs_v" + str(vector_size) + "_w" + str(window_size) + "_s" \
            + str(sample_size) + "_c" + str(min_count) + "_b" + str(batch_size) + "_epoch"
epoch_size = args.epoch
epoch_cur  = 0
epoch_bef  = 0
epoch_loss = 0
count      = 0
interval   = int(words_size / batch_size / 5)

start3   = time.time()
iterator = myiterator.WindowIterator(data_novel, window_size, batch_size)
iterator.set_converter(args.gpu)
while epoch_cur < epoch_size:
    if epoch_cur != epoch_bef:
        now = time.time()
        mean_loss = epoch_loss / count
        print("Epoch:", epoch_cur, "-- mean-loss:", mean_loss, "(", epoch_loss, "/", count, ")")
        print("time:", now-start2)
        model = gconv.convert_model(model, -1)
#        S.save_npz(model_file+"_epoch"+str(epoch_cur)+".npz", model)
        observer.evaluate_word2vec(model, epoch_cur)
#        save_word2vec(model_file, vector_file, epoch_cur)
        model = gconv.convert_model(model)
        epoch_loss = 0
        count      = 0
    model.cleargrads()
    xt, yt = iterator.get_next_batch()
    xt_val = chainer.Variable(xt)
    yt_val = chainer.Variable(yt)
    loss   = model(xt_val, yt_val)
    loss.backward()
    optimizer.update()
    epoch_loss += loss.data
    count      += 1
    epoch_bef   = epoch_cur
    epoch_cur   = iterator.get_epoch()
    if count%interval == 0:
        print("loss:", loss.data, "(", str(count//interval), "/", "5 )")
now = time.time()
mean_loss = epoch_loss / count
print("Epoch:", epoch_cur, "-- mean-loss:", mean_loss, "(", epoch_loss, "/", count, ")")
print("time:", now-start2)
model = gconv.convert_model(model, -1)
#S.save_npz(model_file+"_epoch"+str(epoch_size)+".npz", model)
observer.evaluate_word2vec(model, epoch_size)
#save_word2vec(model_file, vector_file, epoch_size)
