# -*- coding: utf-8 -*-

import numpy as np
import chainer
import chainer.functions as F
import chainer.links as L
import chainer.initializers as I

import filearranger


## Continuous Bag-of-Words model
class CBOW(chainer.Chain):
    def __init__(self, vocab_size, vector_size, loss_func=None):
        if not loss_func:
            loss_func = SoftmaxCrossEntropyFunction(vector_size, vocab_size)
        i_weight_e = I.Uniform(1.0/vector_size)
        super(CBOW, self).__init__(
            hidden=L.EmbedID(vocab_size, vector_size, initialW=i_weight_e),
            loss_func=loss_func,
        )
#        self.hidden.W.data[...] = np.random.uniform(-0.1, 0.1, self.hidden.W.data.shape)


    def __call__(self, context, focus):
        c_vector     = self.hidden(context)
        c_vector_ave = F.sum(c_vector, axis=1) * (1.0/context.data.shape[1])
        loss = self.loss_func(c_vector_ave, focus)

        return loss


    def get_vector(self, x):
        vector = self.hidden(x)
        return vector.data





## Skip-gram model
class SG(chainer.Chain):
    def __init__(self, vocab_size, vector_size, loss_func=None):
        if not loss_func:
            loss_func = SoftmaxCrossEntropyFunction(vector_size, vocab_size)
        i_weight_e = I.Uniform(1.0/vector_size)
        super(SG, self).__init__(
            hidden=L.EmbedID(vocab_size, vector_size, initialW=i_weight_e),
            loss_func=loss_func,
        )


    def __call__(self, focus, context):
        c_vector = self.hidden(context)
        c_shape  = c_vector.data.shape
        c_vector = F.reshape(c_vector, (c_shape[0]*c_shape[1], c_shape[2]))
        f = F.broadcast_to(focus, (c_shape[0], c_shape[1]))
        f = F.reshape(f, (c_shape[0]*c_shape[1],))
        loss = self.loss_func(c_vector, f)
        return loss


    def get_vector(self, x):
        vector = self.hidden(x)
        return vector.data





class SoftmaxCrossEntropyFunction(chainer.Chain):
    def __init__(self, input_num, output_num):
        super(SoftmaxCrossEntropyFunction, self).__init__(
            output=L.Linear(input_num, output_num, initialW=0),
        )


    def __call__(self, x, t):
        return F.softmax_cross_entropy(self.output(x), t)





# My trained Word2Vec
class Word2Vec():
    def __init__(self):
        self.arranger  = filearranger.FileArranger()
        self.word2idx  = {}
        self.idx2word  = {}
        self.word_vecs = []


    def construct_w2v(self, data_file):
        data_list = self.arranger.read_file(data_file)
        for data in data_list:
            elements = data.split("__")
            word     = elements[0]
            word_num = int(elements[1])
            vector   = np.array([float(val) for val in elements[2:]])
            self.word2idx[word]     = word_num
            self.idx2word[word_num] = word
            self.word_vecs.append(vector)


    def get_word_vector_info(self, word=None, word_num=None):
        if word != None:
            word_num = self.word2idx[word]
            return word, word_num, self.word_vecs[word_num]
        elif word_num != None:
            word = self.idx2word[word_num]
            return word, word_num, self.word_vecs[word_num]
        else:
            print("You have to set word or word number!")
            return None, None, None


    def get_word_vector(self, word=None, word_num=None):
        if word != None:
            word_num = self.word2idx[word]
            return self.word_vecs[word_num]
        elif word_num != None:
            return self.word_vecs[word_num]
        else:
            print("You have to set word or word number!")
            return None






if __name__ == "__main__":
    model = SG(100, 200)

    x_val = chainer.Variable(np.array([[5], [6]], dtype=np.int32))
    y_val = chainer.Variable(np.array([[3, 4, 6, 7], [4, 5, 7, 8]], dtype=np.int32))
    print(model(x_val, y_val))
