import numpy as np

import chainer
import chainer.functions as F
import chainer.links as L
import chainer.initializers as I
import converter


class WindowIterator(chainer.dataset.Iterator):
    def __init__(self, dataset, window_size, batch_size, has_repeat=True):
        self.dataset      = np.array(dataset, np.int32)
        self.w_size       = window_size
        self.b_size       = batch_size
        self.has_repeat   = has_repeat
        self.order        = np.random.permutation(len(dataset)-2*window_size).astype(np.int32)
        self.current_pos  = 0
        self.epoch        = 0
        self.is_new_epoch = False
        self.order += window_size


    def set_converter(self, device):
        self.gconv = converter.GPUConverter(device)


    def get_next_batch(self, device=None):
        i_start = self.current_pos
        i_end   = i_start + self.b_size
        pos_cen = self.order[i_start:i_end]
        window  = np.random.randint(self.w_size-1) + 1
        offset  = np.concatenate([np.arange(-window, 0), np.arange(1, window+1)])
        pos_con = pos_cen[:, None] + offset[None, :]
        center  = self.dataset.take(pos_cen)
        center  = self.gconv.convert_data(np.reshape(center, (center.shape[0], 1)), device)
        context = self.gconv.convert_data(self.dataset.take(pos_con), device)
        if i_end >= len(self.order):
            np.random.shuffle(self.order)
            self.epoch       += 1
            self.current_pos  = 0
        else:
            self.current_pos  = i_end
        return center, context


    def get_epoch(self):
        return self.epoch


    def get_count(self):
        return self.current_pos
