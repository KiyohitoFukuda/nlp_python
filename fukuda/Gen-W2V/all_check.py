# -*- coding:utf-8 -*-

from gensim.models import doc2vec
import argparse
import codecs,glob
import shutil
#import filearranger


parser = argparse.ArgumentParser()
parser.add_argument("--v_size", "-v", type=int, default=200, help="size of document (word) vector")
parser.add_argument("--w_size", "-w", type=int, default=10, help="size of window")
parser.add_argument("--c_size", "-c", type=int, default=5, help="size of minimum word counts")
parser.add_argument("--alpha", "-a", type=float, default=0.5, help="rate to simulate target 1")
parser.add_argument("--worker", "-work", type=int, default=4, help="size of workers")
args = parser.parse_args()


#コサイン類似度 -- メソッド化
def cossim(x,y):
    result = 0.
    xL=0.
    yL=0.
    for i in range(len(x)):
        result += x[i]*y[i]
        xL += x[i]**2.0
        yL += y[i]**2.0
    result /= xL**0.5
    result /= yL**0.5
    return result

# テキスト読み込み
#doc_text = doc2vec.TaggedLineDocument('docData_wakati_ao-cookpad-trip.txt')
#doc_text = doc2vec.TaggedLineDocument('docData100.txt')

# ファイル名前読み込み（結果表示に使用する）
#with codecs.open('docName_ao-cookpad-trip.txt', 'r', encoding='utf-8') as f:
#with codecs.open('docName100.txt', 'r', encoding='utf-8') as f:
    #doc_names = [doc_name.replace('\n', '') for doc_name in f.readlines()]
    #doc_names = [doc_name.replace('\r\n','') for doc_name in f.readlines()]

# モデルデータ作成
# sizeはベクトル圧縮時の次元数
# windowは解析時に対象単語の前後何単語まで見るか
# min_countは出現回数が何回以下の単語を無視するか
# workerはスレッド数
#model = doc2vec.Doc2Vec(doc_text, size=100, window=8, min_count=2, workers=4)
#model_size = 200
#model_window = 10
#model_min_c = 2
#model_workers = 4
#print "======= size = "+str(model_size)+ ", window = "+str(model_window)+", min_count = "+str(model_min_c)+", workers = "+str(model_workers)+" ======"
#model = doc2vec.Doc2Vec(doc_text, size=model_size, window=model_window, min_count=model_min_c, workers=model_workers)
# データ保存
#model.save('novel.model')
#model.save_word2vec_format('novel.w2vmodel')#単語のベクトル表現

#vectors = model.docvecs #学習した分散表現
#これは今回がたまたま最後２つがターゲットと言うだけ．本当はこんな書き方はできない！

#ukiwa_vec = vectors[-2]
#usagi_vec = vectors[-1]
#print doc_names[-2],ukiwa_vec
#print doc_names[-1],usagi_vec

#arranger = filearranger.FileArranger()


# doc2vec のモデル読み込み
model_dir = "../MODEL/Gen-W2V/"
model_file     = model_dir + "d2v-model_v" + str(args.v_size) + "_w" + str(args.w_size) \
               + "_c" + str(args.c_size) + ".model"
m = doc2vec.Doc2Vec.load(model_file)

# テキストタグ読み込み（結果表示に使用する）
tag_file = "../DATA/Gen-W2V/hoshi_doctag_u.txt"
#doc_tags = arranger.read_file(tag_file)
doc_tags = []
with open(tag_file, "r", encoding="utf-8") as f:
    for line in f:
        doc_tags.append(line.replace("\n", ""))

# 目標となるテキストの分散表現
#test_doc_name = 'formatted_hebito_kaeru.txt' # 類似文書を求めたいファイル名 夢野久作の蛇と蛙
test_doc_tag    = "formatted_kokushikan_satsujin_jiken.txt"
target_doc_tag1 = "formatted_kokoro.txt" # 類似文書を求めたいファイル名
target_doc_tag2 = "formatted_glass_dono_uchi.txt"
doc_index       = doc_tags.index(test_doc_tag)
doc_index1      = doc_tags.index(target_doc_tag1)
doc_index2      = doc_tags.index(target_doc_tag2)
target_vec1     = m.docvecs[doc_index1]
target_vec2     = m.docvecs[doc_index2]

print(test_doc_tag, ":", doc_index)
print(target_doc_tag1, ":", doc_index1)
print(target_doc_tag2, ":", doc_index2)


# 未知テキスト読み込み
#outfiles 以下に全パターンの分あり

files    = glob.glob("../DATA/Gen-W2V/outfiles/out*.txt")
maxValue = 0 #最大類似度
for f in files:
    with open(f, "r", encoding="utf-8") as unknown_f:
        unknown_f_read = unknown_f.readline()
        i=0
        unknown_doc_text =[]
        for i in range(0,len(unknown_f_read.split(' '))):
            unknown_doc_text.append(unknown_f_read.split(' ')[i])
            i=i+1
###################
#   未知文との距離  #
###################
#では、コーパスに含まれない文を対象として、似ている文を探すとしたら、どうするか？含まれない文のベクトルを推定するのだそうである。
#まず、対象とする文を分かち書きし、リストにしておく。
#次に、ロードしておいたモデルmを使って、m.infer_vector(対象となる文)でベクトルを推定する。xがベクトル。
#このベクトルxをリストにして、most_similarに食わせる。
#(goo.gl/3T03iiより引用)
    input_vec = m.infer_vector(unknown_doc_text) # inputした文章の分散表現
    sim1      = cossim(target_vec1,input_vec)
    sim2      = cossim(target_vec2,input_vec)
    total_sim = args.alpha*sim1 + (1.0-args.alpha)*sim2
#    print(f, input_vec)
#    print("similarity:", sim)
    if total_sim > maxValue:
        maxValue = total_sim
        print(f, input_vec)
        print("max update:", f)
        print("similarity:", total_sim, "(sim1:", sim1, "sim2:", sim2, ")")
        max_file_name = str(f)
print("MAXcosSim:", max_file_name)
print("copy :", max_file_name)
res = shutil.copy(max_file_name,".")
