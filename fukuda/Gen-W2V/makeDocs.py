# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 14:21:13 2016
サイエンスインカレ用 文書の部分部分を変更する
@author: mori
"""

import sys,codecs

f = open(sys.argv[1])
#データは分かち書きされた一行
outname = "out"
data = f.readline()
target=[range(81,83),range(127,132),range(208,211),range(269,273),range(349,355),range(453,454),
        range(533,536),range(858,859),range(1040,1046),range(1234,1237),range(1480,1482)]
tindex = [[81,83],[127,132],[208,211],[269,273],[349,355],[453,454],[533,536],[858,859],[1040,1046],[1234,1237],[1480,1482]]
check = [81,127,208,269,349,453,533,858,1040,1234,1480]
check_end = [83,132,211,273,355,454,536,859,1046,1237,1482]
sentence=[[[u'たくさん',u'の'],[u'いろんな']], [[u'で',u'めざめ',u'て',u'しまい',u'ます'],[u'に',u'おこさ',u'れ',u'て',u'しまう',u'の',u'でし',u'た']],[[u'どういう',u'わけ',u'か'],[u'なぜ',u'か',u'今日',u'は']],[[u' 大',u'好き',u'な',u'の'],[u' とっても',u'好き',u'な',u'の']],[[u'いったい',u'なに',u'が',u'いい',u'の',u'か'],[u'なに',u'が',u'いい',u'の',u'か',u'ちっとも']],[[u'ボロい'],[u'古い']],[[u'いとおし',u'そう',u'に'],[u'キラキラ',u'し',u'た',u'目',u'で']],[[u'ごくろうさま'],[u'ありがとう']],[[u'とっても',u'うれしそう',u'な',u'顔',u'の',u'女の子'],[u'にっこり',u'と',u'わらう',u'女の子',u'の',u'顔']],[[u'おとな',u'に',u'なっても'],[u'これから',u'も']],[[u'ちょっぴり',u'だけ'],[u'ほんの',u'少し']]]
#flag = [0,0,0,0,0,0,0,0,0,0,0] #どちらを選ぶか
#flag=[1,1,1,1,1,1,1,1,1,1,1]

def partOfArray(array,indexArray):
    result = []
    for i in indexArray:
        result.append(array[i])
    return result
    
def makeFlag(x):
    result = []
    for i in range(2**x):
        tmp = list(str(bin(i))[2:])
        print tmp #20161206
        sys.exit()
        tmp.reverse()
        tmp = map(int,tmp)
        for j in range(x-len(tmp)):
            tmp.append(0)
        result.append(tmp)
    return result

"""
いろんな 
に おこさ れ て しまう の でし た 
なぜ か 今日 は 
とっても 好き な の 
なに が いい の か ちっとも 
古い 
キラキラ し た 目 で 
ありがとう 
これから も 
ほんの 少し 
"""


words = data.split(" ")
total = len(words)
allFlags = makeFlag(len(check)) #2^check 個のバイナリパターンをすべて生成
tnum = 2
tindex = 0
for flag in allFlags:
    index = 0
    num = 0 #何番目の修正か
    result = u''
    while index < total:
        if check.count(index):
            tmp = sentence[num][flag[num]]
            result = result + u' '.join(tmp)
            index = check_end[num]
            num += 1
            continue
        result = result + u" " + unicode(words[index],"utf8")
        index += 1
    outf = outname+"".join(map(str,flag))+".txt"
    with codecs.open(outf,'w','utf8') as of:
        of.write(result)
  

