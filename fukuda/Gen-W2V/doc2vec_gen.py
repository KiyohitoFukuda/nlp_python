# -*- coding:utf-8 -*-

from gensim.models import doc2vec
import argparse
import time
import filearranger


parser = argparse.ArgumentParser()
parser.add_argument("--v_size", "-v", type=int, default=200, help="size of document (word) vector")
parser.add_argument("--w_size", "-w", type=int, default=10, help="size of window")
parser.add_argument("--c_size", "-c", type=int, default=5, help="size of minimum word counts")
parser.add_argument("--worker", "-work", type=int, default=4, help="size of workers")
args = parser.parse_args()


arranger    = filearranger.FileArranger()

# テキスト読み込み
input_file = "../DATA/Gen-W2V/hoshi_doc2vec_wakati_u.txt"
doc_text   = doc2vec.TaggedLineDocument(input_file)

# テキストタグ読み込み（結果表示に使用する）
tag_file = "../DATA/Gen-W2V/hoshi_doctag_u.txt"
doc_tags = arranger.read_file(tag_file)
#with open(input_tag_file, "r", encoding="utf-8") as f:
#    doc_tags = [doc_name for doc_name in f]



# 未知テキスト読み込み
#unknown_DocName = './InputData/velveteen.txt'
#unknown_DocName = './InputData/Input.txt'
#with codecs.open(unknown_DocName,'r',encoding= 'utf-8') as unknown_f:
    #unknown_f_read = unknown_f.readline()
    #i=0
    #unknown_doc_text =[]
    #for i in range(0,len(unknown_f_read.split(' '))):
        #unknown_doc_text.append(unknown_f_read.split(' ')[i])
        #i=i+1

# モデルデータ作成
# sizeはベクトル圧縮時の次元数
# windowは解析時に対象単語の前後何単語まで見るか
# min_countは出現回数が何回以下の単語を無視するか
# workerはスレッド数
model_size = args.v_size
model_window = args.w_size
model_min_c = args.c_size
model_workers = args.worker

start1 = time.time()
print("======= size = "+str(model_size)+", window = "+str(model_window)+", min_count = "+str(model_min_c)+", workers = "+str(model_workers)+" ======")
model = doc2vec.Doc2Vec(doc_text, size=model_size, window=model_window, min_count=model_min_c, workers=model_workers)
now = time.time()
print("time: ", now-start1)

# データ保存
model_dir = "../MODEL/Gen-W2V/"
arranger.make_directory(model_dir)
model_file     = model_dir + "d2v-model_v" + str(model_size) + "_w" + str(model_window) \
               + "_c" + str(model_min_c) + ".model"
w2v_model_file = model_dir + "w2v-model_v" + str(model_size) + "_w" + str(model_window) \
               + "_c" + str(model_min_c) + ".model"
model.save(model_file)
model.save_word2vec_format(w2v_model_file)#単語のベクトル表現

"""
#ここから単語同士の類似度
word = u'蛙' # 類似単語を求めたい単語
print(word+' is similar to...')
# 類似単語と類似度のタプル（類似度上位10件）のリストを受け取る
for similarity in model.most_similar(positive=word):
    # タプルのままIPythonに出力するとリテラル表示されないのでこの書き方
    print(similarity[0], similarity[1])
"""

###################
#   未知文との距離  #
###################
#では、コーパスに含まれない文を対象として、似ている文を探すとしたら、どうするか？含まれない文のベクトルを推定するのだそうである。
#まず、対象とする文を分かち書きし、リストにしておく。
#次に、ロードしておいたモデルmを使って、m.infer_vector(対象となる文)でベクトルを推定する。xがベクトル。
#このベクトルxをリストにして、most_similarに食わせる。
#(goo.gl/3T03iiより引用)
#m = model.load('novel.model')
#input_vector = m.infer_vector(unknown_doc_text) # inputした文章の分散表現
#unknown_similar_docs = m.docvecs.most_similar([input_vector],topn=15)
#print unknown_similar_docs
#print(unknown_DocName+' is similar to...')
#for unknown_similar_doc in unknown_similar_docs:
    #print(doc_names[unknown_similar_doc[0]], unknown_similar_doc[1])

"""
###################
#   既知文との距離  #
###################
#test_doc_name = 'formatted_hebito_kaeru.txt' # 類似文書を求めたいファイル名 夢野久作の蛇と蛙
#test_doc_name = 'formatted_gan.txt' # 類似文書を求めたいファイル名 森鴎外 雁
#doc_index = doc_names.index(test_doc_name)

# 類似カードと類似度のタプル（類似度上位10件）のリストを受け取る
#topnで上位何件を取るか指定
#similar_docs = model.docvecs.most_similar(doc_index, topn = 10)
#print model.docvecs.similarity(0,1)#similarity Function:引数はdoc_namesのインデックス. 文書間の類似度を計算

#print('\n')
#print(doc_names[doc_index]+' is similar to...')
#for similar_doc in similar_docs:
    #print(doc_names[similar_doc[0]], similar_doc[1])
"""
"""
###################
#    ベクトル出力   #
###################
#VectorFile = open('novel_vector.txt','w')
#vectors = model.docvecs

#for i in range(0,len(doc_names)):
    #VectorFile.write(doc_names[i]+' ')
    #VectorFile.write(str(vectors[i])+'\n')
#VectorFile.write('\n'+unknown_DocName+' '+str(input_vector))
"""
"""
#類似ベクトルのみの書き込み，必要であれば使用する
#for similar_doc in similar_docs:
    #VectorFile.write(doc_names[similar_doc[0]] )
    #VectorFile.write(str(similar_docs[2][2])+'\n')
    #print(doc_names[similar_doc[0]], similar_docs[2]) #ベクトル表現を取得
"""
#VectorFile.close()
