# -*- coding: utf-8 -*-

import os
from mecab import Mecab
import argparse


class FileArranger():
    def __init__(self):
        self.name = "FileArranger"


    def wakati_file(self, input_file_name):
        mecab = Mecab()
        output_file_name = mecab.getWakatiFile(input_file_name)
        return output_file_name


    def merge_file(self, input_file_names, output_file_name, iencoding="utf-8", oencoding="utf-8"):
        with open(output_file_name, "w", encoding=iencoding) as output:
            all = 0
            for file_name in input_file_names:
                count = 0;
                with open(file_name, "r", encoding=oencoding) as f:
                    for line in f:
                        output.write(str(line))
                        count += 1
                print(file_name, ":", count)
                all += count
            print("merge", str(all), "sentences")


    def read_file(self, input_file_name, iencoding="utf-8"):
        sentences = []
        with open(input_file_name, "r", encoding=iencoding) as f:
            for line in f:
                sentences.append(line.replace("\n", ""))
        return sentences


    def write_file(self, output_file_name, data, oencoding="utf-8"):
        with open(output_file_name, "w", encoding=oencoding) as output:
            for line in data:
                output.write(str(line))


    def get_absolute_path(self,  root=True):
        if root:
            return os.path.abspath(os.path.dirname(__file__) + "\\..")
        else:
            return os.path.abspath(os.path.dirname(__file__))


    def searchFileList(self, dir_name):
        list = os.listdir(dir_name)
        files = []
        for l in list:
            if os.path.isfile(dir_name + l):
                files.append(dir_name + l)
        return files


    def make_directory(self, dir_name):
        if not os.path.isdir(dir_name):
            os.makedirs(dir_name)
            print("create directory:", dir_name)
