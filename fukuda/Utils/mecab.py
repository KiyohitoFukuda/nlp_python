# -*- coding: utf-8 -*-

import subprocess as proc
import sys
import io
import os


class Mecab():
    def __init__(self):
        self.name   = "Mecab"
        self.char   = "utf-8"
        self.data   = []
        self.str    = "NO_DATA"
        self.length = 0
        self.temp1  = "../Utils/temp_mec.txt"
        self.temp2  = "../Utils/temp_mec_result.txt"

        if not sys.stdout.encoding == "utf-8":
            print(sys.stdout.encoding, "\t(mecab)")
            sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')


## sentence を Mecab にかけ, 解析結果を self.data に登録する.
    def set_mecab_data(self, sentence):
        sentence = sentence.replace("\t", "　")
        sentence = sentence.replace(",", "、")
        sentence = sentence.replace(".", "。")
        self.str = sentence
        self.data = []
        print("a")

        with open(self.temp1, "w", encoding=self.char) as f1:
            f1.write(self.str)
        cmd = []
        cmd.append("mecab")
        cmd.append("-U%M\tunknown,unknown,*,*,*,*,%M,*,*,69\n")
        cmd.append("-F%M\t%H,%h\n")
        cmd.append(self.temp1)
        with open(self.temp2, "w", encoding=self.char) as f2:
            output = proc.Popen(cmd, stdin=proc.PIPE, stdout=f2).wait()
        with open(self.temp2, "r", encoding=self.char) as f3:
            for line in f3:
                line = line.rstrip()
                if line != "EOS":
                    self.data.append(line)



## 基本形を返す.
    def get_lemma(self):
        output = []
        for line in self.data:
            output.append(line.split(",")[6])

        return output


## 表層表現を返す
    def get_surface(self):
        output = []
        for line in self.data:
            output.append(line.split("\t")[0])

        return output


## 品詞情報を返す.
    def get_pos(self, isDeep=True):
        output = []
        for line in self.data:
            l = line.split("\t")[1]
            pos  = "-"  + l.split(",")[0]
            pos1 = "-"  + l.split(",")[1]
            pos2 = "-"  + l.split(",")[2]
            if isDeep:
                pos += pos1 + pos2
            pos += "-"
            output.append(pos)

        return output


## 品詞番号を返す.
    def get_pos_number(self):
        output = []
        for line in self.data:
            pos_num = line.split(",")[9]
            if pos_num == "*":
                pos_num = "69"
            output.append(pos_num)

        return output


## 活用型を返す.
    def get_conjugated_type(self):
        output = []
        for line in self.data:
            output.append(line.split(",")[4])

        return output


## 活用形を返す.
    def get_conjugated_form(self):
        output = []
        for line in self.data:
            output.append(line.split(",")[5])

        return output


## 発音を返す.
    def get_pronunciation(self):
        output = []
        for line in self.data:
            output.append(line.split(",")[8])

        return output


## 読みを返す.
    def get_reading(self):
        output = []
        for line in self.data:
            output.append(line.split(",")[7])

        return output


## 分かち書きしたものを返す.
    def get_wakati(self):
        output = []
        for line in self.data:
            output.append(line.split("\t")[0])
        wakati = " ".join(output)

        return wakati


## input_ile 内の文字列を分かち書きし, output_file に保存する.
    def get_wakati_file(self, input_file, output_file):
        cmd = ["mecab", "-Owakati", input_file]
        process = 1
        with open(output_file, "w") as f:
            process = proc.Popen(cmd, stdout=f).wait()
        while process != 0:
            print(process + "not 0")




if __name__ == "__main__":
    mecab = Mecab()

    s = "用意は良いですか？　それでは、始め！"
    mecab.set_mecab_data(s)
    lemma = mecab.get_lemma()
    print(s)
    print(lemma)
