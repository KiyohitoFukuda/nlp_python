# -*- coding: utf-8 -*-

import filearranger

arranger = filearranger.FileArranger()

# for Wiki data
"""
input_files = ["../DATA/Word2Vec/raw_wakati_u1.txt", "../DATA/Word2Vec/raw_wakati_u2.txt",
               "../DATA/Word2Vec/raw_wakati_u3.txt", "../DATA/Word2Vec/raw_wakati_u4.txt",
               "../DATA/Word2Vec/raw_wakati_u5.txt", "../DATA/Word2Vec/raw_wakati_u6.txt",
               "../DATA/Word2Vec/raw_wakati_u7.txt", "../DATA/Word2Vec/raw_wakati_u8.txt",
               "../DATA/Word2Vec/raw_wakati_u9.txt", "../DATA/Word2Vec/raw_wakati_u10.txt",
               "../DATA/Word2Vec/raw_wakati_u11.txt", "../DATA/Word2Vec/raw_wakati_u12.txt",
               "../DATA/Word2Vec/raw_wakati_u13.txt", "../DATA/Word2Vec/raw_wakati_u14.txt",
               "../DATA/Word2Vec/raw_wakati_u15.txt", "../DATA/Word2Vec/raw_wakati_u16.txt",
               "../DATA/Word2Vec/raw_wakati_u17.txt", "../DATA/Word2Vec/raw_wakati_u18.txt",
               "../DATA/Word2Vec/raw_wakati_u19.txt", "../DATA/Word2Vec/raw_wakati_u20.txt",
               "../DATA/Word2Vec/raw_wakati_u21.txt", "../DATA/Word2Vec/raw_wakati_u22.txt",
               "../DATA/Word2Vec/raw_wakati_u23.txt"]

output_file = "../DATA/wiki_wakati_u.txt"
arranger.merge_file(input_files, output_file)
"""

# for wiki data with aozora, cookpad and trip adviser
# with tag data
input_files1 = ["../DATA/Word2Vec/raw_wakati_u1.txt", "../DATA/Word2Vec/raw_wakati_u2.txt",
               "../DATA/Word2Vec/raw_wakati_u3.txt", "../DATA/Word2Vec/raw_wakati_u4.txt",
               "../DATA/Word2Vec/raw_wakati_u5.txt", "../DATA/Word2Vec/raw_wakati_u6.txt",
               "../DATA/Word2Vec/raw_wakati_u7.txt", "../DATA/Word2Vec/raw_wakati_u8.txt",
               "../DATA/Word2Vec/raw_wakati_u9.txt", "../DATA/Word2Vec/raw_wakati_u10.txt",
               "../DATA/Word2Vec/raw_wakati_u11.txt", "../DATA/Word2Vec/raw_wakati_u12.txt",
               "../DATA/Word2Vec/raw_wakati_u13.txt", "../DATA/Word2Vec/raw_wakati_u14.txt",
               "../DATA/Word2Vec/raw_wakati_u15.txt", "../DATA/Word2Vec/raw_wakati_u16.txt",
               "../DATA/Word2Vec/raw_wakati_u17.txt", "../DATA/Word2Vec/raw_wakati_u18.txt",
               "../DATA/Word2Vec/raw_wakati_u19.txt", "../DATA/Word2Vec/raw_wakati_u20.txt",
               "../DATA/Word2Vec/raw_wakati_u21.txt", "../DATA/Word2Vec/raw_wakati_u22.txt",
               "../DATA/Word2Vec/raw_wakati_u23.txt"]
output_file1 = "../DATA/Word2Vec/wiki_wakati_u.txt"
#arranger.merge_file(input_files1, output_file1)

tags = []
wiki_tag_file = "../DATA/Gen-W2V/wiki_tag_u.txt"
wiki_data = arranger.read_file(output_file1)
for i in range(len(wiki_data)):
    tags.append("w_" + str(i) + "\n")
arranger.write_file(wiki_tag_file, tags)

input_files2 = [output_file1, "../DATA/Gen-W2V/docData_wakati_ao-cp-tr1500_u.txt"]
output_file2 = "../DATA/Gen-W2V/hoshi_doc2vec_wakati_u.txt"
arranger.merge_file(input_files2, output_file2)

input_tag_files  = [wiki_tag_file, "../DATA/Gen-W2V/docName_ao-cp-tr1500_u.txt"]
output_tag_file = "../DATA/Gen-W2V/hoshi_doctag_u.txt"
arranger.merge_file(input_tag_files, output_tag_file)
