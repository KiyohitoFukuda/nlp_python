import numpy as np
import chainer
from chainer import cuda

class GPUConverter():
    def __init__(self, device):
        self.device = device
        if device >= 0:
            chainer.cuda.get_device_from_id(device).use()
            cuda.check_cuda_available()


    def convert_data(self, data, device=None):
        if device == None:
            device = self.device
        new_data = data
        if device >= 0:
            new_data = cuda.to_gpu(data)
        else:
            new_data = cuda.to_cpu(data)
        return new_data


    def convert_model(self, model, device=None):
        if device == None:
            device = self.device
        new_model = model
        if device >= 0:
            new_model.to_gpu()
        else:
            new_model.to_cpu()
        return new_model
