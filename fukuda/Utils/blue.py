import numpy as np
import math



class BLUE():
    def __init__(self):
        pass


    def get_blue_score(self, original, trans, ngram=4):
        o_length = len(original)
        t_length = len(trans)

        bp = 1
        if o_length > t_length:
            bp = math.exp(1-o_length/t_length)
        if o_length < ngram:
            ngram = o_length
        if t_length < ngram:
            ngram = t_length
        scores = [0.0] * ngram
        for i in range(ngram):
            n   = i + 1
            val = 0
            gram_vocab  = {}
            taboo_vocab = {}
            for j in range(o_length-n+1):
                data = "-".join([str(s) for s in original[j:j+n]])
                if data in gram_vocab:
                    gram_vocab[data] += 1
                else:
                    gram_vocab[data] = 1
            for j in range(t_length-n+1):
                data = "-".join([str(s) for s in trans[j:j+n]])
                if data not in taboo_vocab:
                    taboo_vocab[data] = 1
                    if data in gram_vocab:
                        val += gram_vocab[data]
            if val != 0:
                scores[n-1] += math.log(val/(t_length-n+1)) / ngram
            else:
                scores[n-1] = 1.0
        score = 0.0
        miss  = 0
        for s in scores:
            if s > 0.5:
                miss += 1
            else:
                score += s

        if miss != ngram:
            return bp * math.exp(score)
        else:
            return 0.0


if __name__ == "__main__":
    b_eval = BLUE()
    val = b_eval.get_blue_score([1, 2], [4, 3])
    print(val)
