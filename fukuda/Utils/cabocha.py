# -*- coding: utf-8 -*-

import subprocess as proc
import sys
import io
import os



class CaboCha():
    def __init__(self):
        self.name    = "Cabocha"
        self.char    = "utf-8"
        self.data    = []
        self.str     = "NO_DATA"
        self.length  = 0
        self.temp1   = "temp_cab.txt"
        self.temp2   = "temp_cab_result.txt"
        self.seg_num  = 0

        if not sys.stdout.encoding == "utf-8":
            print(sys.stdout.encoding, "\t(cabocha)")
            sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')


## sentence を Cabocha にかけ, 解析結果を self.data に登録する.
    def set_cabocha_data(self, sentence):
        sentence = sentence.replace("\t", "　")
        sentence = sentence.replace(",", "、")
        sentence = sentence.replace(".", "。")
        self.str = sentence
        self.data    = []
        self.seg_num = 0

        with open(self.temp1, "w", encoding=self.char) as f:
            f.write(self.str)
        cmd = []
        cmd.append("cabocha")
        cmd.append("-f1")
        cmd.append(self.temp1)
        with open(self.temp2, "w", encoding=self.char) as f1:
            output = proc.Popen(cmd, stdin=proc.PIPE, stdout=f1).wait()

        with open(self.temp2, "r", encoding=self.char) as f2:
            for line in f2:
                line = line.rstrip()
                if line != "EOS":
                    self.data.append(line)
                if line.startswith("*"):
                    self.seg_num += 1


## 各文節ごとの基本形を返す.
    def get_segment_lemma(self):
        output = [[] for i in range(self.seg_num)]
        count  = -1
        for line in self.data:
            if line.startswith("*"):
                count += 1
                continue
            output[count].append(line.split(",")[6])
        return output


    def get_segment_size(self):
        return self.seg_num




if __name__ == "__main__":
    cabocha = CaboCha()

    s = "太郎君は橋の上で泳いでいる花子さんを見つけた。"
    cabocha.set_cabocha_data(s)
    lemma = cabocha.get_segment_lemma()
    print(s)
    print(lemma)
