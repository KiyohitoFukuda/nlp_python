import numpy as np
import copy
import filearranger as filer





class SimLoader():
    def __init__(self, agent_num):
        self.sim_data = []
        self.sentence = []
        self.a_size   = agent_num
        self.d_size   = 1
        self.ex_idx1  = [2, 4]
        self.ex_idx2  = [3, 5]
        self.filer    = filer.FileArranger()


    def load_simulation(self, data_file, type="nor"):
        is_first  = True
        count     = 0
        data_pos  = []
        data_rate = []
        dataset   = []
        sim_data  = None
        data_list = self.filer.read_file(data_file, iencoding="shift_JIS")
        for data in data_list:
            elements = data.split(",")
            if is_first:
                is_first = False
                for i in range(len(elements)):
                    if "座標" in elements[i]:
                        data_pos.append(i)
                        data_rate.append(0.01)
                    if "機嫌" in elements[i]:
                        data_pos.append(i)
                        if type == "rev":
                            data_rate.append(-1.0)
                        else:
                            data_rate.append(1.0)
                    if "友好度" in elements[i]:
                        data_pos.append(i)
                        if type == "rev":
                            data_rate.append(-0.01)
                        else:
                            data_rate.append(0.01)
                    if "認知度" in elements[i]:
                        data_pos.append(i)
                        data_rate.append(0.01)
                    if "アイテム" in elements[i]:
                        data_pos.append(i)
                        data_rate.append(1.0)
                self.d_size = len(data_pos)
                sim_data = [0.0] * self.d_size*self.a_size
                continue
            order = count%self.a_size
            if type == "cha":
                for i in len(self.ex_idx1):
                    if order == self.ex_idx1[i]:
                        order = self.ex_idx2[i]
            for i, idx in enumerate(data_pos):
                if elements[idx] == "true":
                    sim_data[self.d_size*order+i] = 1.0 * data_rate[i]
                elif elements[idx] == "false":
                    sim_data[self.d_size*order+i] = 0.0 * data_rate[i]
                else:
                    sim_data[self.d_size*order+i] = float(elements[idx]) * data_rate[i]
            count += 1
            if order+1 == self.a_size:
                temp = copy.deepcopy(sim_data)
                dataset.append(temp)
                sim_data = [0.0] * self.d_size*self.a_size
        self.sim_data.append(np.array(dataset))


    def generate_simulation_data(self, type="con"):
        inputs  = []
        outputs = []
        for i in range(len(self.sentence)):
            before = np.zeros(len(self.sim_data[i][0]), dtype=np.float32)
            sentence = self.sentence[i]
            simulation_input_data  = []
            simulation_output_data = []
            for j in range(len(sentence)):
                if type == "con":
                    temp = np.r_[before, self.sim_data[i][j]]
                    simulation_input_data.append(temp)
                elif type == "dif":
                    temp =self.sim_data[i][j] - before
                    simulation_input_data.append(temp)
                before = np.copy(self.sim_data[i][j])
                words = sentence[j]
                joined_words = "".join(words)
                if joined_words == "変化なし":
                    simulation_output_data.append(np.array([0], dtype=np.int32))
                else:
                    simulation_output_data.append(np.array([1], dtype=np.int32))
            inputs.append(np.array(simulation_input_data))
            outputs.append(np.array(simulation_output_data))

        return np.array(inputs), np.array(outputs)


    def load_sentence(self, sentence_file):
        is_first = True
        sentences = self.filer.read_file(sentence_file, iencoding="shift_JIS")
        sentence = []
        for data in sentences:
            elements = data.split(",")
            if is_first:
                is_first = False
                continue
            words  = elements[1].strip().split(" ")
            sentence.append(words)
        self.sentence.append(sentence)


    def generate_sentence_data(self, vocab):
        inputs  = []
        outputs = []
        for sentence in self.sentence:
            input_sentence_data  = []
            output_sentence_data = []
            for words in sentence:
                temp_i = np.zeros(len(words)+1)
                temp_o = np.zeros(len(words)+1)
                temp_i[0]          = vocab["<S>"]
                temp_o[len(words)] = vocab["</S>"]
                for i, word in enumerate(words):
                    temp_i[i+1] = vocab[word]
                    temp_o[i]   = vocab[word]
                input_sentence_data.append(temp_i)
                output_sentence_data.append(temp_o)
            inputs.append(np.array(input_sentence_data))
            outputs.append(np.array(output_sentence_data))

        return np.array(inputs), np.array(outputs)


    def make_vocab(self):
        index = 0
        vocab = {}
        index2word = []
        vocab["<S>"] = index
        index2word.append("<S>")
        index += 1
        vocab["</S>"] = index
        index2word.append("</S>")
        index += 1
        for sentence in self.sentence:
            for words in sentence:
                for word in words:
                    if not word in vocab:
                        vocab[word] = index
                        index2word.append(word)
                        index += 1

        return vocab, index2word


    def get_sentence(self):
        return self.sentence





class NovelLoader():
    def __init__(self, vocab_max, min_count):
        self.vocab_max  = vocab_max
        self.min_count  = min_count
        self.novel_dict = {}
        self.unknown    = "UNK"
        self.index      = 1
        self.vocab    = {self.unknown: 0}
        self.re_vocab = {0: self.unknown}
        self.count    = {self.unknown: 0}
        self.idxs_novel    = []
        self.idxs_sentence = []
        self.filer = filer.FileArranger()


    def read(self, input_file):
        sentences = self.filer.read_file(input_file)
        self.novel_dict[input_file] = sentences


    def make_vocabulary(self):
        temp = {}
        for file_name in self.novel_dict:
            sentences = self.novel_dict[file_name]
            for sentence in sentences:
                words = sentence.split()
                words.insert(0, "<S>")
                words.append("</S>")
                for word in words:
                    if word in temp:
                        temp[word] += 1
                    else:
                        temp[word] = 1
            for w in temp:
                if temp[w] >= self.min_count:
                    self.vocab[w]             = self.index
                    self.re_vocab[self.index] = w
                    self.count[w]             = temp[w]
                    self.index               += 1
                else:
                    self.count[self.unknown] += temp[w]
        return self.vocab, self.re_vocab, self.count


    def load_vocabulary(self, dict_file):
        self.vocab    = {}
        self.re_vocab = {}
        self.count    = {}

        dict_data = self.filer.read_file(dict_file)
        for data in dict_data:
            elements = data.split(" ")
            self.vocab[elements[0]]         = int(elements[1])
            self.re_vocab[int(elements[1])] = elements[0]
            self.count[elements[0]]         = int(elements[2])
        return self.vocab, self.re_vocab, self.count


    def make_dataset(self, vocab):
        idxs_novel    = []
        idxs_sentence = []
        for file_name in self.novel_dict:
            sentences = self.novel_dict[file_name]
            for sentence in sentences:
                words = sentence.split()
                words.insert(0, "<S>")
                words.append("</S>")
                sent2idx = [0] * len(words)
                for i, word in enumerate(words):
                    if word in vocab:
                        sent2idx[i] = vocab[word]
                    else:
                        sent2idx[i] = vocab[self.unknown]
                self.idxs_novel.extend(sent2idx)
                self.idxs_sentence.append(sent2idx)
        return self.idxs_novel, self.idxs_sentence


    def get_vocab_size(self):
        return len(self.vocab)

    def get_words_size(self):
        return len(self.idxs_novel)
