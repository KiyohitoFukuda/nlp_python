import numpy as np


class EncoderIterator():
    def __init__(self, i_dataset, o_dataset, batch_size, has_repeat=True):
        self.i_dataset    = i_dataset
        self.o_dataset    = o_dataset
        self.b_size       = batch_size
        self.has_repeat   = has_repeat
        self.order        = np.random.permutation(len(i_dataset)).astype(np.int32)
        self.current_pos  = 0
        self.epoch        = 0
        self.is_new_epoch = False


    def set_converter(self, device):
        self.gconv = converter.GPUConverter(device)


    def get_lstm_batch(self, device=None):
        batch_i = []
        batch_o = []
        temp_d  = {}
        start   = self.current_pos
        end     = start + self.b_size
        order   = self.order[start:end]
        for i in order:
            tenp_d[i] = self.i_dataset[i]
        for item in reversed(sorted(temp_d.items(), key=lambda x:len(x[1]))):
            batch_i.append(item[1])
            batch_o.append(self.o_dataset[item[0]])
            batch_i = np.array(batch_i, dtype=np.float32)
            batch_o = np.array(batch_o, dtype=np.int32)

        if end >= len(self.order):
            np.random.shuffle(self.order)
            self.epoch       += 1
            self.current_pos  = 0
        else:
            self.current_pos  = end
        return batch_i, batch_o


    def get_epoch(self):
        return self.epoch


    def get_position(self):
        return self.current_pos


    def get_order(self):
        return self.order


    def set_order(self, order):
        self.order = order





class DecoderIterator():
    def __init__(self, i_dataset, o_dataset, batch_size, has_repeat=True):
        self.i_dataset    = i_dataset
        self.o_dataset    = o_dataset
        self.b_size       = batch_size
        self.has_repeat   = has_repeat
        self.order        = np.random.permutation(len(i_dataset)).astype(np.int32)
        self.current_pos  = 0
        self.epoch        = 0
        self.is_new_epoch = False


    def set_converter(self, device):
        self.gconv = converter.GPUConverter(device)


    def get_lstm_batch(self, device=None):
        batch_i = []
        batch_o = []
        temp_d  = {}
        start   = self.current_pos
        end     = start + self.b_size
        order   = self.order[start:end]
        for i in order:
            tenp_d[i] = self.i_dataset[i]
        for item in reversed(sorted(temp_d.items(), key=lambda x:len(x[1]))):
            batch_i.append(item[1])
            batch_o.append(self.o_dataset[item[0]])
            batch_i = self.converter.convert_data(np.array(batch_i, dtype=np.int32))
            batch_o = self.converter.convert_data(np.array(batch_o, dtype=np.int32))

        if end >= len(self.order):
            np.random.shuffle(self.order)
            self.epoch       += 1
            self.current_pos  = 0
        else:
            self.current_pos  = end
        return batch_i, batch_o


    def get_epoch(self):
        return self.epoch


    def get_position(self):
        return self.current_pos


    def get_order(self):
        return self.order


    def set_order(self, order):
        self.order = order
