import numpy as np

import chainer
import time

import chainer.links as L
import chainer.functions as F
import chainer.initializers as I
import chainer.optimizers as O
import chainer.serializers as S

from chainer import cuda

import downloader
import encoderdecoder
import filearranger
import blue


class Evaluator():
    def __init__(self):
        self.model_enc = None
        self.model_dec = None
        self.size_enc  = ""
        self.size_dec  = ""
        self.i_enc_tr, self.i_enc_te = (None, None)
        self.o_enc_tr, self.o_enc_te = (None, None)
        self.i_dec_tr, self.i_dec_te = (None, None)
        self.o_dec_tr, self.o_dec_te = (None, None)


    def set_encoder(self, model_enc, size_list_enc):
        self.model_enc = model_enc
        for size in size_list_enc:
            self.size_enc += "-" + str(size)


    def set_decoder(self, model_dec, size_list_dec):
        self.model_dec = model_dec
        for size in size_list_dec:
            self.size_dec += "-" + str(size)


    def get_result_encoder(self, inputs_enc, outputs_enc):
        ans0_num, ans1_num = (0, 0)
        r_ans0_num, r_ans1_num = (0, 0)
        o_ans0_num, o_ans1_num = (0, 0)
        data_size = 0
        result_list = []

        for i in range(len(inputs_enc)):
            i_enc      = inputs_enc[i]
            o_enc      = outputs_enc[i]
            data_size += len(i_enc)
            self.model_enc.reset_state()
            for j in range(len(i_enc)):
                i_data = chainer.Variable(np.array([i_enc[j]], dtype=np.float32))
                o_data = chainer.Variable(np.array([o_enc[j]], dtype=np.int32))
                result, hidden = self.model_enc.encode(i_data)
                if result == 0:
                    ans0_num += 1.0
                    if o_data.data[0] == 0:
                        o_ans0_num += 1.0
                        r_ans0_num += 1.0
                    elif o_data.data[0] == 1:
                        o_ans1_num += 1.0
                elif result == 1:
                    ans1_num += 1.0
                    if o_data.data[0] == 1:
                        o_ans1_num += 1.0
                        r_ans1_num += 1.0
                    elif o_data.data[0] == 0:
                        o_ans0_num += 1.0
            precision = r_ans1_num / ans1_num
            recall    = r_ans1_num / o_ans1_num
            f_value   = (2*precision*recall) / (precision+recall)
            acc       = (r_ans0_num+r_ans1_num) / data_size
            baseline  = o_ans0_num / (o_ans0_num+o_ans1_num)
        print("precision:", precision, "\t"+str(r_ans1_num)+"/"+str(ans1_num))
        print("recall:", recall, "\t"+str(r_ans1_num)+"/"+str(o_ans1_num))
        print("f_measure:", f_value, "\t"+str(2*precision*recall)+"/"+str(precision+recall))
        print("accuracy:", acc, "\t"+str(r_ans0_num+r_ans1_num)+"/"+str(data_size))
        print("baseline:", baseline, "\t"+str(o_ans0_num)+"/"+ str(o_ans0_num+o_ans1_num))
        result_list.append("識別率: "+str(acc)+"\t"+str(r_ans0_num+r_ans1_num)+"/"+str(data_size) + "\n")
        result_list.append("ベースライン: "+str(baseline)+"\t"+str(o_ans0_num)+"/"+ str(o_ans0_num+o_ans1_num) + "\n")
        result_list.append("適合率: "+str(precision)+"\t"+str(r_ans1_num)+"/"+str(ans1_num) + "\n")
        result_list.append("再現率: "+str(recall)+"\t"+str(r_ans1_num)+"/"+str(o_ans1_num) + "\n")
        result_list.append("F 値: "+str(f_value)+"\t"+str(2*precision*recall)+"/"+str(precision+recall) + "\n")

        return result_list


    def get_result_decoder(inputs_enc, outputs_enc, inputs_dec, outputs_dec, sentences, vocab, idx2word):
        b_eval = blue.Blue()
        result_list = []
        result_list.append("ID,enc正解,enc出力,BLUE値,正解,出力\n")
        for i in range(len(inputs_dec)):
            i_enc    = inputs_enc[i]
            o_enc    = outputss_enc[i]
            i_dec    = inputs_dec[i]
            o_dec    = outputs_dec[i]
            sentence = sentences[i]
            self.model_enc.reset_state()
            for j in range(len(data)):
                i_data = chainer.Variable(np.array([i_enc[j]], dtype=np.float32))
                result, h_data = self.model_enc.encode(i_data)
                o_data = self.model_dec.decode(h_data, vocab, idx2word)
                answer = " ".join(sentence[j])
                output = " ".join(o_data)
                score  = b_eval(answer, output, ngram=3)
                result_list.append(str(i)+"-"+str(j) + "," + str(o_enc[j]) + "," + str(result) + "," + str(score) + "," + answer + "," + o_data + "\n")

        return result_list


    def evaluate_encoder(self, i_enc_tr, o_enc_tr, i_enc_te, o_enc_te):
        self.i_enc_tr = i_enc_tr
        self.i_enc_te = i_enc_te
        self.o_enc_tr = o_enc_tr
        self.o_enc_te = o_enc_te
        arranger   = filearranger.FileArranger()
        result_enc = []
        result_enc_name = "../RESULT/encoder_result_size" + self.size_enc
        result_enc.extend(self.get_result_encoder(i_enc_tr, o_enc_tr))
        result_enc.extend(self.get_result_encoder(i_enc_te, o_enc_te))
        arranger.write_file(result_enc_name, result_enc)


    def evaluate_decoder(self, i_dec_tr, o_dec_tr, i_dec_te, o_dec_te, sent_tr, sent_te, vocab, idx2word):
        arranger   = filearranger.FileArranger()
        train_result_name = "../RESULT/decoder_result_train_size" + self.size_dec + ".csv"
        test_result_name  = "../RESULT/decoder_result_test_size" + self.size_dec + ".csv"
        arranger.write_file(train_result_name, self.get_result_decoder(self.i_enc_tr, self.o_enc_tr, i_dec_tr, o_dec_tr, sent_tr, vocab, idx2word))
        arranger.write_file(train_result_name, self.get_result_decoder(self.i_enc_te, self.o_enc_te, i_dec_te, o_dec_te, sent_te, vocab, idx2word))
