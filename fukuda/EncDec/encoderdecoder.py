import numpy as np

import chainer
import chainer.links as L
import chainer.functions as F
import chainer.initializers as I
import converter


"""
class Decoder_RNN(chainer.Chain):
    def __init__(self, converter, vocab_size, vector_size):
        i_weight_e = I.Uniform(1/vector_size)
        i_weight_l = I.HeNormal()
        i_weight_u = I.HeNormal()
        i_weight_o = I.HeNormal()
        init_keywords = {"lateral_init": i_weight_l, "upward_init": i_weight_u}
        super(Decoder_RNN, self).__init__(
            hidden1=L.EmbedID(vocab_size, vector_size, initialW=i_weight_e),
            hidden2=L.LSTM(vector_size, vector_size, **init_keywords),
            output=L.Linear(vector_size, vocab_size, initialW=i_weight_o),
        )
        self.converter = converter


    def __call__(self, inputs, answers, hidden_input):
        loss_sum = None
        self.hidden2.reset_state()
        self.hidden2.h = hidden_input
        for i in range(len(inputs)):
            data = chainer.Variable(self.converter.convert_data(np.array([inputs[i]], dtype=np.int32)))
            ans  = chainer.Variable(self.converter.convert_data(np.array([answers[i]], dtype=np.int32)))
            h_vector1 = self.hidden1(data)
            h_vector2 = self.hidden2(h_vector1)
            output_vector = self.output(h_vector2)
            loss = F.softmax_cross_entropy(output_vector, ans)
            if loss_sum is None:
                loss_sum = loss
            else:
                loss_sum = loss_sum + loss

        return loss_sum


    def decode(self, hidden_input, vocab, index2word):
        self.hidden2.reset_state()
        self.hidden2.h = hidden_input
        output     = []
        input_data = "<S>"
        while not input_data in "</S>":
            data = chainer.Variable(self.converter.convert_data(np.array([vocab[input_data]], dtype=np.int32)))
            h_vector1 = self.hidden1(data)
            h_vector2 = self.hidden2(h_vector1)
            output_vector = self.output(h_vector2)
            max_index = np.argmax(output_vector.data)
            input_data = index2word[max_index]
            output.append(input_data)

        return output
"""





class Decoder_RNN(chainer.Chain):
    def __init__(self, vocab_size, vector_size):
        i_weight_e = I.Uniform(1/vector_size)
        i_weight_l = I.HeNormal()
        i_weight_u = I.HeNormal()
        i_weight_o = I.HeNormal()
        init_keywords = {"lateral_init": i_weight_l, "upward_init": i_weight_u}
        super(Decoder_RNN, self).__init__(
            hidden1=L.EmbedID(vocab_size, vector_size, initialW=i_weight_e),
            hidden2=L.LSTM(vector_size, vector_size, **init_keywords),
            output=L.Linear(vector_size, vocab_size, initialW=i_weight_o),
        )
        self.size = str(vocab_size) + "-" + str(vector_size) + "-" + str(vocab_size)


    def __call__(self, i_data, o_data, hidden_input=None):
        if isinstance(hidden_input, chainer.Variable):
            self.hidden2.reset_state()
            self.hidden2.h = hidden_input
        h_vector1 = self.hidden1(i_data)
        h_vector2 = self.hidden2(h_vector1)
        output_vector = self.output(h_vector2)
        loss = F.softmax_cross_entropy(output_vector, o_data)
        return loss


    def decode(self, i_data, hidden_input=None):
        if isinstance(hidden_input, chainer.Variable):
            self.hidden2.reset_state()
            self.hidden2.h = hidden_input
        h_vector1 = self.hidden1(i_data)
        h_vector2 = self.hidden2(h_vector1)
        output_vector = self.output(h_vector2)
        max_index = np.argmax(output_vector.data)
        return max_index


    def get_model_size(self):
        return self.size





class Decoder_NstepLSTM(chainer.Chain):
    def __init__(self, vocab_size, vector_size, layer_size):
        i_weight_e = I.Uniform(1/vector_size)
        i_weight_o = I.HeNormal()
        super(Decoder_NstepLSTM, self).__(
            embed=L.EmbedID(vocab_size, vector_size, initialW=i_weight_e),
            n_lstm=L.NStepLSTM(layer_size, vector_size, vector_size),
            output=L.Linear(vector_size, vocab_size, initialW=i_weight_o),
        )
        self.size =  str(vocab_size) + "-" + str(vector_size) + "-" + str(vocab_size) + "-" + str(layer_size)

    def __call__(self, i_dataset, a_dataset, hidden_input=None):
        cell_input = None
        h_vector1 = [self.embed(i_data) for i_data in i_dataset]
        h_vector2 = self.n_lstm(hidden_input, cell_input, h_vector1)
        o_vector  = [self.output(i_data) for i_data in i_dataset]
        return o_vector


    def get_model_size(self):
        return self.size





class Encoder_MLP(chainer.Chain):
    def __init__(self, input_size, hidden_size, output_size):
        i_weight_h = I.HeNormal()
        i_weight_o = I.HeNormal()
        super(Encoder_MLP, self).__init__(
            hidden=L.Linear(input_size, hidden_size, initialW=i_weight_h),
            output=L.Linear(hidden_size, output_size, initialW=i_weight_o),
        )


    def __call__(self, input_data):
        h_vector = self.hidden(input_data)
        o_vector = self.output(F.relu(h_vector))

        return F.mean_squared_error(o_vector, input_data)


    def encode(self, input_data):
        h_vector = self.hidden(input_data)
        return h_vector





class Encoder_RNN(chainer.Chain):
    def __init__(self, input_size, hidden1_size, hidden2_size, output_size, rate):
        i_weight_l = I.HeNormal()
        i_weight_u = I.HeNormal()
        i_weight_h = I.HeNormal()
        i_weight_o = I.HeNormal()
        init_keywords = {"lateral_init": i_weight_l, "upward_init": i_weight_u}
        super(Encoder_RNN, self).__init__(
            hidden1=L.LSTM(input_size, hidden1_size, **init_keywords),
            hidden2=L.Linear(hidden1_size, hidden2_size, initialW=i_weight_h),
            output=L.Linear(hidden2_size, output_size, initialW=i_weight_o),
        )
#        self.rate = rate
        self.rate = 1.0
        self.size = str(input_size) + "-" + str(hidden1_size) + "-" + str(hidden2_size) + "-" + str(output_size)


    def __call__(self, i_data, o_data, is_first=False):
        if is_first:
            self.hidden1.reset_state()

        h1_vector = self.hidden1(i_data)
        h2_vector = self.hidden2(F.elu(h1_vector))
        o_vector = self.output(F.elu(h2_vector))
        loss = F.softmax_cross_entropy(o_vector, o_data)
        acc  = F.accuracy(o_vector, o_data)
        if o_data.data[0] == 1:
            loss = loss * self.rate
        return loss, acc


    def encode(self, i_data, is_first=False):
        if is_first:
            self.hidden1.reset_state()

        h1_vector = self.hidden1(i_data)
        h2_vector = self.hidden2(F.elu(h1_vector))
        o_vector = self.output(F.elu(h2_vector))
        return np.argmax(o_vector.data), h1_vector


    def get_model_size(self):
        return self.size



class Encoder_NStepLSTM(chainer.Chain):
    def __init__(self, layer_size, input_size, vector_size, hidden_size, output_size, rate):
        i_weight_l = I.HeNormal()
        i_weight_u = I.HeNormal()
        i_weight_h = I.HeNormal()
        i_weight_o = I.HeNormal()
        init_keywords = {"lateral_init": i_weight_l, "upward_init": i_weight_u}
        super(Encoder_RNN, self).__init__(
            hidden1=L.LSTM(input_size, hidden1_size, **init_keywords),
            hidden2=L.Linear(hidden1_size, hidden2_size, initialW=i_weight_h),
            output=L.Linear(hidden2_size, output_size, initialW=i_weight_o),
        )
#        self.rate = rate
        self.rate = 1.0
        self.size = str(input_size) + "-" + str(hidden1_size) + "-" + str(hidden2_size) + "-" + str(output_size)
