import filearranger
import blue



arranger = filearranger.FileArranger()
b_eval   = blue.BLUE()
test_name1 = "../TEST/decoder_result_size161.csv"
test_name2 = "../TEST/decoder_result_size322.csv"

test_data_list1 = arranger.read_file(test_name1)
test_data_list2 = arranger.read_file(test_name2)
test_data_lists = [arranger.read_file(test_name1), arranger.read_file(test_name2)]



for test_data_list in test_data_lists:
    t_p_score_te, t_n_score_te = (0.0, 0.0)
    t_p_mean_te,  t_n_mean_te  = (0.0, 0.0)
    t_p_count_te, t_n_count_te = (0, 0)
    f_p_score_te, f_n_score_te = (0.0, 0.0)
    f_p_mean_te,  f_n_mean_te  = (0.0, 0.0)
    f_p_count_te, f_n_count_te = (0, 0)
    t_p_score_tr, t_n_score_tr = (0.0, 0.0)
    t_p_mean_tr,  t_n_mean_tr  = (0.0, 0.0)
    t_p_count_tr, t_n_count_tr = (0, 0)
    f_p_score_tr, f_n_score_tr = (0.0, 0.0)
    f_p_mean_tr,  f_n_mean_tr  = (0.0, 0.0)
    f_p_count_tr, f_n_count_tr = (0, 0)
    is_first = True
    for test_data in test_data_list:
        if is_first:
            is_first = False
            continue
        number1 = test_data.split(",")[0].split("-")[0]
        number2 = test_data.split(",")[0].split("-")[1]
        t_or_f  = test_data.split(",")[1]
        p_or_n  = test_data.split(",")[2]
        refs    = test_data.split(",")[3]
        trans   = test_data.split(",")[4]
        ref_data   = refs.split(" ")
        trans_data = trans.split(" ")
        b_score    = b_eval.get_blue_score(ref_data, trans_data[1:-1])
        if t_or_f == "[1]":
            if p_or_n == "1":
                if int(number1) < 900:
                    t_p_score_te += b_score
                    t_p_count_te += 1
                elif int(number1) >= 900:
                    t_p_score_tr += b_score
                    t_p_count_tr += 1
            else:
                if int(number1) < 900:
                    t_n_score_te += b_score
                    t_n_count_te += 1
                elif int(number1) >= 900:
                    t_n_score_tr += b_score
                    t_n_count_tr += 1
        else:
            if p_or_n == "1":
                if int(number1) < 900:
                    f_p_score_te += b_score
                    f_p_count_te += 1
                elif int(number1) >= 900:
                    f_p_score_tr += b_score
                    f_p_count_tr += 1
            else:
                if int(number1) < 900:
                    f_n_score_te += b_score
                    f_n_count_te += 1
                elif int(number1) >= 900:
                    f_n_score_tr += b_score
                    f_n_count_tr += 1

        if int(number1)%1000==0 and int(number2)==0:
            if t_p_count_te != 0:
                t_p_mean_te = t_p_score_te / t_p_count_te
            if t_n_count_te != 0:
                t_n_mean_te = t_n_score_te / t_n_count_te
            if f_p_count_te != 0:
                f_p_mean_te = f_p_score_te / f_p_count_te
            if f_n_count_te != 0:
                f_n_mean_te = f_n_score_te / f_n_count_te
            if t_p_count_tr != 0:
                t_p_mean_tr = t_p_score_tr / t_p_count_tr
            if t_n_count_tr != 0:
                t_n_mean_tr = t_n_score_tr / t_n_count_tr
            if f_p_count_tr != 0:
                f_p_mean_tr = f_p_score_tr / f_p_count_tr
            if f_n_count_tr != 0:
                f_n_mean_tr = f_n_score_tr / f_n_count_tr
            print("(TEST) True-Positive:", t_p_mean_te, "(", t_p_score_te, "/", t_p_count_te, ")")
            print("(TEST) True-Negative:", t_n_mean_te, "(", t_n_score_te, "/", t_n_count_te, ")")
            print("(TEST) False-Positive:", f_p_mean_te, "(", f_p_score_te, "/", f_p_count_te, ")")
            print("(TEST) Flase-Negative:", f_n_mean_te, "(", f_n_score_te, "/", f_n_count_te, ")")
            print("(TRAIN) True-Positive:", t_p_mean_tr, "(", t_p_score_tr, "/", t_p_count_tr, ")")
            print("(TRAIN) True-Negative:", t_n_mean_tr, "(", t_n_score_tr, "/", t_n_count_tr, ")")
            print("(TRAIN) False-Positive:", f_p_mean_tr, "(", f_p_score_tr, "/", f_p_count_tr, ")")
            print("(TRAIN) Flase-Negative:", f_n_mean_tr, "(", f_n_score_tr, "/", f_n_count_tr, ")")
            print()
    if t_p_count_te != 0:
        t_p_mean_te = t_p_score_te / t_p_count_te
    if t_n_count_te != 0:
        t_n_mean_te = t_n_score_te / t_n_count_te
    if f_p_count_te != 0:
        f_p_mean_te = f_p_score_te / f_p_count_te
    if f_n_count_te != 0:
        f_n_mean_te = f_n_score_te / f_n_count_te
    if t_p_count_tr != 0:
        t_p_mean_tr = t_p_score_tr / t_p_count_tr
    if t_n_count_tr != 0:
        t_n_mean_tr = t_n_score_tr / t_n_count_tr
    if f_p_count_tr != 0:
        f_p_mean_tr = f_p_score_tr / f_p_count_tr
    if f_n_count_tr != 0:
        f_n_mean_tr = f_n_score_tr / f_n_count_tr
    all_score_te = t_p_score_te + t_n_score_te + f_p_score_te + f_n_score_te
    all_count_te = t_p_count_te + t_n_count_te + f_p_count_te + f_n_count_te
    all_score_tr = t_p_score_tr + t_n_score_tr + f_p_score_tr + f_n_score_tr
    all_count_tr = t_p_count_tr + t_n_count_tr + f_p_count_tr + f_n_count_tr
    all_mean_te = all_score_te / all_count_te
    all_mean_tr = all_score_tr / all_count_tr
    print("(TEST) True-Positive:", t_p_mean_te, "(", t_p_score_te, "/", t_p_count_te, ")")
    print("(TEST) True-Negative:", t_n_mean_te, "(", t_n_score_te, "/", t_n_count_te, ")")
    print("(TEST) False-Positive:", f_p_mean_te, "(", f_p_score_te, "/", f_p_count_te, ")")
    print("(TEST) Flase-Negative:", f_n_mean_te, "(", f_n_score_te, "/", f_n_count_te, ")")
    print("(TEST) ALL:", all_mean_te, "(", all_score_te, "/", all_count_te, ")")
    print("(TRAIN) True-Positive:", t_p_mean_tr, "(", t_p_score_tr, "/", t_p_count_tr, ")")
    print("(TRAIN) True-Negative:", t_n_mean_tr, "(", t_n_score_tr, "/", t_n_count_tr, ")")
    print("(TRAIN) False-Positive:", f_p_mean_tr, "(", f_p_score_tr, "/", f_p_count_tr, ")")
    print("(TRAIN) Flase-Negative:", f_n_mean_tr, "(", f_n_score_tr, "/", f_n_count_tr, ")")
    print("(TRAIN) ALL:", all_mean_tr, "(", all_score_tr, "/", all_count_tr, ")")
    print("\n")


"""
is_first = True
for test_data1, test_data2 in zip(test_data_list1, test_data_list2):
    if is_first:
        is_first = False
        continue

    number = test_data1.split(",")[0]
    refs1  = test_data1.split(",")[3]
    trans1 = test_data1.split(",")[4]
    refs2  = test_data2.split(",")[3]
    trans2 = test_data2.split(",")[4]
    ref_data1   = refs1.split(" ")
    trans_data1 = trans1.split(" ")
    ref_data2   = refs2.split(" ")
    trans_data2 = trans2.split(" ")
    b_score1    = b_eval.get_blue_score(ref_data1, trans_data1[1:-1])
    b_score2    = b_eval.get_blue_score(ref_data2, trans_data2[1:-1])
    print(number, b_score1, b_score2)
"""
