import numpy as np
import chainer
import time
import chainer.links as L
import chainer.functions as F
import chainer.initializers as I
import chainer.optimizers as O
import chainer.serializers as S
from chainer import cuda
import downloader
import encoderdecoder
import filearranger
import blue


class Observer():
    def __init__(self, args):
        self.i_enc_tr, self.i_enc_te = (None, None)
        self.o_enc_tr, self.o_enc_te = (None, None)
        self.i_dec_tr, self.i_dec_te = (None, None)
        self.o_dec_tr, self.o_dec_te = (None, None)
        self.sent_tr, self.sent_te   = (None, None)
        self.vocab    = None
        self.idx2word = None
        self.args = args
        self.type = args.type1 + "-" + args.type2
        self.arranger = filearranger.FileArranger()


    def set_encode_data(self, i_enc_tr, o_enc_tr, i_enc_te, o_enc_te):
        self.i_enc_tr = i_enc_tr
        self.o_enc_tr = o_enc_tr
        self.i_enc_te = i_enc_te
        self.o_enc_te = o_enc_te


    def set_decode_data(self, i_dec_tr, o_dec_tr, i_dec_te, o_dec_te, sent_tr, sent_te, vocab, idx2word):
        self.i_dec_tr = i_dec_tr
        self.o_dec_tr = o_dec_tr
        self.i_dec_te = i_dec_te
        self.o_dec_te = o_dec_te
        self.sent_tr  = sent_tr
        self.sent_te  = sent_te
        self.vocab    = vocab
        self.idx2word = idx2word


    def get_result_encoder(self, model_enc, input_enc_list, output_enc_list):
        ans0_num, ans1_num = (0, 0)
        r_ans0_num, r_ans1_num = (0, 0)
        o_ans0_num, o_ans1_num = (0, 0)
        precision, recall      = (0, 0)
        f_value   = 0
        data_size = 0
        result_list = []

        for inputs_enc, outputs_enc in zip(input_enc_list, output_enc_list):
            data_size += len(inputs_enc)
            is_first   = True
            for i_enc, o_enc in zip(inputs_enc, outputs_enc):
                i_data = chainer.Variable(np.array([i_enc], dtype=np.float32))
                o_data = chainer.Variable(np.array(o_enc, dtype=np.int32))
                result, hidden = model_enc.encode(i_data, is_first)
                is_first = False
                if result == 0:
                    ans0_num += 1.0
                    if o_data.data[0] == 0:
                        o_ans0_num += 1.0
                        r_ans0_num += 1.0
                    elif o_data.data[0] == 1:
                        o_ans1_num += 1.0
                elif result == 1:
                    ans1_num += 1.0
                    if o_data.data[0] == 1:
                        o_ans1_num += 1.0
                        r_ans1_num += 1.0
                    elif o_data.data[0] == 0:
                        o_ans0_num += 1.0
            if ans1_num != 0:
                precision = r_ans1_num / ans1_num
            if o_ans1_num != 0:
                recall    = r_ans1_num / o_ans1_num
            if precision!=0 or recall!=0:
                f_value   = (2*precision*recall) / (precision+recall)
            acc       = (r_ans0_num+r_ans1_num) / data_size
            baseline  = o_ans0_num / (o_ans0_num+o_ans1_num)
        print("precision:", precision, "\t"+str(r_ans1_num)+"/"+str(ans1_num))
        print("recall:", recall, "\t"+str(r_ans1_num)+"/"+str(o_ans1_num))
        print("f_measure:", f_value, "\t"+str(2*precision*recall)+"/"+str(precision+recall))
        print("accuracy:", acc, "\t"+str(r_ans0_num+r_ans1_num)+"/"+str(data_size))
        print("baseline:", baseline, "\t"+str(o_ans0_num)+"/"+ str(o_ans0_num+o_ans1_num))
        result_list.append("識別率: "+str(acc)+"\t"+str(r_ans0_num+r_ans1_num)+"/"+str(data_size) + "\n")
        result_list.append("ベースライン: "+str(baseline)+"\t"+str(o_ans0_num)+"/"+ str(o_ans0_num+o_ans1_num) + "\n")
        result_list.append("適合率: "+str(precision)+"\t"+str(r_ans1_num)+"/"+str(ans1_num) + "\n")
        result_list.append("再現率: "+str(recall)+"\t"+str(r_ans1_num)+"/"+str(o_ans1_num) + "\n")
        result_list.append("F 値: "+str(f_value)+"\t"+str(2*precision*recall)+"/"+str(precision+recall) + "\n")

        return result_list


    def get_result_decoder(self, model_enc, model_dec, input_enc_list, output_enc_list, input_dec_list, output_dec_list, sentences):
        b_eval = blue.BLUE()
        result_list = []
        result_list.append("ID,enc正解,enc出力,BLUE値,正解,出力\n")
        for i in range(len(input_enc_list)):
            inputs_enc  = input_enc_list[i]
            outputs_enc = output_enc_list[i]
            inputs_dec  = input_dec_list[i]
            outputs_dec = output_dec_list[i]
            sentence    = sentences[i]
            is_first_enc = True
            for j in range(len(inputs_enc)):
                i_enc_data = chainer.Variable(np.array([inputs_enc[j]], dtype=np.float32))
                o_enc_data = chainer.Variable(np.array(outputs_enc[j], dtype=np.int32))
                result, h_data = model_enc.encode(i_enc_data)
                outputs  = []
                is_first_enc = False
                is_first_dec = True
                for i_dec, o_dec in zip(inputs_dec[j], outputs_dec[j]):
                    i_data = chainer.Variable(np.array([i_dec], dtype=np.int32))
                    o_data = chainer.Variable(np.array([o_dec], dtype=np.int32))
                    if is_first_dec:
                        output = model_dec.decode(i_data, h_data)
                    else:
                        output = model_dec.decode(i_data)
                    is_first_dec = False
                    outputs.append(self.idx2word[output])
                answer = " ".join(sentence[j])
                output = " ".join(outputs[:-1])
                score  = b_eval.get_blue_score(sentence[j], outputs[:-1], ngram=4)
                result_list.append(str(i)+"-"+str(j)+","+str(o_enc_data.data)+","+ str(result)+","+ str(score)+","+answer+","+output+"\n")

        return result_list


    def evaluate_encoder(self, model_enc, epoch):
        model_dir = "../MODEL/EncDec/" + self.type + "/"
        self.arranger.make_directory(model_dir)
        model_file = model_dir + "encoder_size" + model_enc.get_model_size() + "_epoch" + str(epoch) + ".npz"
        S.save_npz(model_file, model_enc)

        result_dir  = "../RESULT/EncDec/" + self.type + "/"
        self.arranger.make_directory(result_dir)
        result_file = result_dir + "encoder_size" + model_enc.get_model_size() + "_epoch" + str(epoch)
        result_enc  = []
        result_enc.extend(self.get_result_encoder(model_enc, self.i_enc_tr, self.o_enc_tr))
        result_enc.extend(self.get_result_encoder(model_enc, self.i_enc_te, self.o_enc_te))
        self.arranger.write_file(result_file, result_enc)


    def evaluate_decoder(self, model_enc, model_dec, epoch):
        model_dir = "../MODEL/EncDec/" + self.type + "/"
        self.arranger.make_directory(model_dir)
        model_file = model_dir + "decoder_size" + model_dec.get_model_size() + "_epoch" + str(epoch) + ".npz"
        S.save_npz(model_file, model_dec)

        result_dir = "../RESULT/EncDec/" + self.type + "/"
        self.arranger.make_directory(result_dir)
        tr_result_file = result_dir + "decoder_train_size" + model_dec.get_model_size() + "_epoch" + str(epoch) + ".csv"
        te_result_file = result_dir + "decoder_test_size" + model_dec.get_model_size() + "_epoch" + str(epoch) + ".csv"
        self.arranger.write_file(tr_result_file, self.get_result_decoder(model_enc, model_dec, self.i_enc_tr, self.o_enc_tr, self.i_dec_tr, self.o_dec_tr, self.sent_tr))
        self.arranger.write_file(te_result_file, self.get_result_decoder(model_enc, model_dec, self.i_enc_te, self.o_enc_te, self.i_dec_te, self.o_dec_te, self.sent_te))


    def save_progress(self, model_name, model_size, progress_list):
        progress_dir = "../MODEL/EncDec/" + self.type + "/"
        self.arranger.make_directory(progress_dir)
        progress_file = progress_dir + model_name + "_size" + model_size + ".csv"
        self.arranger.write_file(progress_file, progress_list)
