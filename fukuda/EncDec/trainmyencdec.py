import numpy as np
import argparse
import time
import chainer
from chainer import cuda
import chainer.links as L
import chainer.functions as F
import chainer.initializers as I
import chainer.optimizers as O
import chainer.serializers as S
import downloader
import encoderdecoder as encdec
import filearranger
import observer
import converter


# 引数設定
parser = argparse.ArgumentParser()
parser.add_argument("--type1", "-t1", default="nor", help="Type of generating input data ('''nor''', '''rev''', and '''cha''')")
parser.add_argument("--type2", "-t2", default="con", help="Type of arranging input data ('''con''' or '''dif''')")
parser.add_argument("--epoch1", "-e1", type=int, default=50, help="Number of encoder'''s epoch")
parser.add_argument("--epoch2", "-e2", type=int, default=5, help="Number of decoder'''s epoch")
parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU and positive value indicates GPU)')
args = parser.parse_args()

#if args.gpu > 0:
#    chainer.cuda.get_device_from_id(args.gpu).use()
#    cuda.check_cuda_available()

arranger  = filearranger.FileArranger()
loader    = downloader.SimLoader(7)
gconv     = converter.GPUConverter(args.gpu)
observer = observer.Observer(args)

# データ読み込み
data_dir = "../DATA/"
size0_train = 0
size1_train = 0
size0_test  = 0
size1_test  = 0

start1 = time.time()
for i in range(100):
    filename_data = data_dir + "EncDec/data_" + str(i) + ".csv"
    filename_sent = data_dir + "EncDec/sentence_" + str(i) + ".csv"
    loader.load_simulation(filename_data, type=args.type1)
    loader.load_sentence(filename_sent)
vocab, index2word = loader.make_vocab()
inputs_enc, outputs_enc = loader.generate_simulation_data(type=args.type2)
inputs_dec, outputs_dec = loader.generate_sentence_data(vocab)
sentences = loader.get_sentence()
print("data_size:", len(inputs_enc))
print("vector_size:", inputs_enc[0].shape[1])
print("vocab_size:", len(vocab))
i_enc_train_list = inputs_enc[:90]
o_enc_train_list = outputs_enc[:90]
i_enc_test_list  = inputs_enc[90:]
o_enc_test_list  = outputs_enc[90:]
i_dec_train_list = inputs_dec[:90]
o_dec_train_list = outputs_dec[:90]
i_dec_test_list  = inputs_dec[90:]
o_dec_test_list  = outputs_dec[90:]
sentence_train_list = sentences[:90]
sentence_test_list  = sentences[90:]
observer.set_encode_data(i_enc_train_list, o_enc_train_list, i_enc_test_list, o_enc_test_list)
observer.set_decode_data(i_dec_train_list, o_dec_train_list, i_dec_test_list, o_dec_test_list, sentence_train_list, sentence_test_list, vocab, index2word)
for i in range(len(o_enc_train_list)):
    o_enc = o_enc_train_list[i]
    for j in range(len(o_enc)):
        if o_enc[j][0] == 0:
            size0_train += 1
        else:
            size1_train += 1
for i in range(len(o_enc_test_list)):
    o_enc = o_enc_test_list[i]
    for j in range(len(o_enc)):
        if o_enc[j][0] == 0:
            size0_test += 1
        else:
            size1_test += 1
print("CLASS 0: train --", size0_train, "test -- ", size0_test)
print("CLASS 1: train --", size1_train, "test -- ", size1_test)
print("クラス割合:", str(size0_train/size1_train))
print("経過時間 (データ整形):", str(time.time()-start1))


# モデル作成
enc_i_size  = inputs_enc[0][0].shape[0]
enc_h1_size = 128
enc_h2_size = 32
enc_o_size  = 2
dec_size    = len(vocab)
batch_size  = 0
enc_rate    = size0_train / size1_train

model_enc      = encdec.Encoder_RNN(enc_i_size, enc_h1_size, enc_h2_size, enc_o_size, enc_rate)
model_enc      = gconv.convert_model(model_enc)
model_dec      = encdec.Decoder_RNN(dec_size, enc_h1_size)
model_dec      = gconv.convert_model(model_dec)
optimizer_enc  = O.Adam(alpha=0.0005)
optimizer_dec  = O.Adam(alpha=0.0005)
optimizer_enc.setup(model_enc)
optimizer_dec.setup(model_dec)


"""
save_data = []
for i in range(len(inputs_enc)):
    input_enc = inputs_enc[i]
    for j in range(len(input_enc)):
        i_data = input_enc[j]
        data = ""
        for k in range(len(i_data)):
            data += str(k) + ":" + "{0:.4f}".format(i_data[k]) + ","
        data += "\n"
        save_data.append(data)

data_name = "../MODEL/data_check_size" + str(enc_i_size) + ".txt"
arranger.write_file(data_name, save_data)
"""


# モデル学習 (Encoder)
epoch_enc = args.epoch1
epoch_dec = args.epoch2
type_dir  = args.type1 + "-" + args.type2 + "/"

progress_enc = ["epoch,iter,mean_loss,mean_acc\n"]

start2 = time.time()
for i in range(epoch_enc):
    train_loss = 0
    train_acc = 0
    count = 0
    data_size = 0
    perm = np.random.permutation(range(len(i_enc_train_list)))
    for j in range(len(perm)):
        i_enc_trains = i_enc_train_list[perm[j]]
        o_enc_trains = o_enc_train_list[perm[j]]
        data_size   += len(i_enc_trains)
        is_first = True
        loss_sum = 0
        acc_sum  = 0
        model_enc.cleargrads()
        for i_enc_train, o_enc_train in zip(i_enc_trains, o_enc_trains):
            #print(i_enc_train, o_enc_train)
            i_data = chainer.Variable(gconv.convert_data(np.array([i_enc_train], dtype=np.float32)))
            o_data = chainer.Variable(gconv.convert_data(np.array(o_enc_train, dtype=np.int32)))
            loss, acc = model_enc(i_data, o_data, is_first)
            loss_sum += loss
            acc_sum  += acc
            is_first  = False
        loss_sum.backward()
        loss_sum.unchain_backward()
        optimizer_enc.update()
        train_loss += loss_sum.data
        train_acc += acc_sum.data
        count += 1
#        if count%300 == 0:
#            print("epoch:", i, "\titer:", count)
#            print("loss_mean:", mean_loss, "acc_mean:", mean_acc)
    mean_loss = train_loss / data_size
    mean_acc  = train_acc / data_size
    progress_enc.append(str(i)+","+str(count)+","+str(mean_loss)+","+str(mean_acc)+"\n")
    print("epoch:", i, "loss_mean:", mean_loss, "acc_mean:", mean_acc)
    print("経過時間 (Encoder 学習):", str(time.time()-start2))
    if i%100==0 and i!=0:
        model_enc = gconv.convert_model(model_enc, -1)
        observer.evaluate_encoder(model_enc, i)
        model_enc = gconv.convert_model(model_enc)
print()
print("経過時間 (Encoder 学習):", str(time.time()-start2))

observer.save_progress("encoder", model_enc.get_model_size(), progress_enc)
model_enc = gconv.convert_model(model_enc, -1)
observer.evaluate_encoder(model_enc, epoch_enc)
model_enc = gconv.convert_model(model_enc)


# モデル学習(Decoder)
progress_dec = ["epoch,iter,mean_loss\n"]

start3 = time.time()
for i in range(epoch_dec):
    train_loss = 0
    count = 0
    perm = np.random.permutation(range(len(i_dec_train_list)))
    for j in range(len(perm)):
        i_dec_trains = i_dec_train_list[perm[j]]
        o_dec_trains = o_dec_train_list[perm[j]]
        i_enc_trains = i_enc_train_list[perm[j]]
        o_enc_trains = o_enc_train_list[perm[j]]
        is_first_enc = True
        for k in range(len(i_dec_trains)):
            i_enc_data = chainer.Variable(gconv.convert_data(np.array([i_enc_trains[k]], dtype=np.float32)))
            result, hidden = model_enc.encode(i_enc_data, is_first_enc)
            is_first_enc = False
            is_first_dec = True
            loss_sum = 0
            model_dec.cleargrads()
            i_dec_train = i_dec_trains[k]
            o_dec_train = o_dec_trains[k]
            for i, o in zip(i_dec_train, o_dec_train):
                i_data = chainer.Variable(gconv.convert_data(np.array([i], dtype=np.int32)))
                o_data = chainer.Variable(gconv.convert_data(np.array([o], dtype=np.int32)))
                if is_first_dec:
                    loss = model_dec(i_data, o_data, hidden)
                else:
                    loss = model_dec(i_data, o_data)
                loss_sum += loss
                is_first_dec = False
            loss_sum.backward()
            loss_sum.unchain_backward()
            optimizer_dec.update()
            train_loss += loss_sum.data
            count += 1
#            if count%2000 == 0:
#                print("epoch:", i, "\titer:", count)
#                print("loss_sum:", mean_loss)
    mean_loss = train_loss / count
    progress_dec.append(str(i)+","+str(count)+","+str(mean_loss)+"\n")
    print("epoch:", i, "loss_mean:", mean_loss)
    print("経過時間 (decoder 学習):", str(time.time()-start3))
    if i%10==0 and i!=0:
        model_enc = gconv.convert_model(model_enc, -1)
        model_dec = gconv.convert_model(model_dec, -1)
        observer.evaluate_decoder(model_ENC, model_dec, i)
        model_enc = gconv.convert_model(model_enc)
        model_dec = gconv.convert_model(model_dec)
print("\n")
print("経過時間 (decoder 学習):", str(time.time()-start3))

observer.save_progress("decoder", model_dec.get_model_size(), progress_dec)
model_enc = gconv.convert_model(model_enc, -1)
model_dec = gconv.convert_model(model_dec, -1)
observer.evaluate_decoder(model_enc, model_dec, epoch_dec)
